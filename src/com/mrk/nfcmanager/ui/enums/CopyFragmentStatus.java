package com.mrk.nfcmanager.ui.enums;

public enum CopyFragmentStatus {
	WAITING_FOR_COPY,
	WAITING_FOR_WRITE,
}