package com.mrk.nfcmanager.ui.enums;

public enum WaitForTag {
	READ,
	WRITE,
	DELETE,
	COPY
}