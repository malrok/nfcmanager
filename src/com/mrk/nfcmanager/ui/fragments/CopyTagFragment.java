package com.mrk.nfcmanager.ui.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.mrk.nfcmanager.R;
import com.mrk.nfcmanager.ext.ripplebackground.RippleBackground;
import com.mrk.nfcmanager.ui.HostScreen;
import com.mrk.nfcmanager.ui.adapters.DragDropGridAdapter;
import com.mrk.nfcmanager.ui.enums.CopyFragmentStatus;
import com.mrk.nfcmanager.ui.enums.WaitForTag;
import com.mrk.nfcmanager.ui.fragments.interfaces.FragmentNFCWriteInterface;

public class CopyTagFragment extends Fragment implements FragmentNFCWriteInterface {

	private HostScreen hostActivity;
	
	/* Views */
	private GridView copyTagContent;
	private TextView copySize;
	private RippleBackground writeRippleAnimation;
	
	/* adapters */
	private DragDropGridAdapter adapter;
	
	/* status of the fragment */
	private CopyFragmentStatus status;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.copy_tag_fragment, container, false);
		
		copyTagContent = (GridView) view.findViewById(R.id.copy_grid);
		copySize = (TextView) view.findViewById(R.id.copy_size);
		writeRippleAnimation = (RippleBackground) view.findViewById(R.id.ripple_write);
		
		return view;
	}
	
	@Override
	public void onStart() {
		super.onStart();
		init();
	}
	
	@Override
	public void onStop() {
		writeRippleAnimation.stopRippleAnimation();
		super.onStop();
	}
	
	@Override
	public void writeNFC() {
		if (this.status == CopyFragmentStatus.WAITING_FOR_WRITE) {
			Toast.makeText(getActivity(), this.hostActivity.getResources().getString(R.string.tag_copied), Toast.LENGTH_SHORT).show();
		}
	}
	
	private void init() {
		this.hostActivity = (HostScreen)getActivity();
		adapter = new DragDropGridAdapter(getActivity(), this.hostActivity.getViewModel().getRecordsList().getListData(), 4);
        copyTagContent.setAdapter(adapter);
        
        String copyText = this.hostActivity.getResources().getString(R.string.used_size);
        copyText = copyText.replaceAll("@", String.valueOf(this.hostActivity.getNdefMessage() == null ? 0 : this.hostActivity.getNdefMessage().getByteArrayLength()));
        
        copySize.setText(copyText);
        
        setRippleAnimation();
        
        this.status = CopyFragmentStatus.WAITING_FOR_WRITE;
        this.hostActivity.waitForTag(WaitForTag.WRITE, false);
	}
	
	private void setRippleAnimation() {
		if (status  == CopyFragmentStatus.WAITING_FOR_WRITE) {
			writeRippleAnimation.startRippleAnimation();
		} else {
			writeRippleAnimation.startRippleAnimation();
		}
	}
}
