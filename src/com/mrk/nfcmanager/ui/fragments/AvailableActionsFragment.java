package com.mrk.nfcmanager.ui.fragments;

import java.util.ArrayList;
import java.util.List;

import android.app.Fragment;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;

import com.mrk.nfcmanager.R;
import com.mrk.nfcmanager.dataloaders.InternalActionsDataLoader;
import com.mrk.nfcmanager.helpers.ViewHelper;
import com.mrk.nfcmanager.ui.HostScreen;
import com.mrk.nfcmanager.ui.adapters.AvailableActionsGridAdapter;
import com.mrk.nfcmanager.ui.delegates.ShowcaseElement;
import com.mrk.nfcmanager.ui.fragments.interfaces.FragmentShowcaseInterface;
import com.mrk.nfcmanager.viewmodels.AvailableActionsListViewModel;
import com.mrk.nfcmanager.viewmodels.RecordViewModel;

public class AvailableActionsFragment extends Fragment implements OnItemClickListener, FragmentShowcaseInterface {
	
	private AvailableActionsListViewModel viewModel;
	
	private GridView gridView;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.available_actions_fragment, container, false);
		
		gridView = (GridView) view.findViewById(R.id.available_actions_gridview);
		
		gridView.setOnItemClickListener(this);
		
		return view;
	}

	@Override
	public void onStart() {
		super.onStart();
		
		viewModel = new AvailableActionsListViewModel();
		viewModel.loadFromDataLoader(getActivity(), new InternalActionsDataLoader());
		
		AvailableActionsGridAdapter adapter = new AvailableActionsGridAdapter(getActivity().getApplicationContext(), viewModel);
		
        gridView.setAdapter(adapter);
	}
	
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		((HostScreen)getActivity()).getViewModel().addRecordToList(new RecordViewModel(getActivity().getApplicationContext()).clone(viewModel.getListData().get(position)));

		Rect initialBounds = new Rect(view.getLeft(), view.getTop(), view.getRight(), view.getBottom());
		
		BitmapDrawable viewBitmap = ViewHelper.getBitmapDrawableFromView(view);
		viewBitmap.setBounds(new Rect(initialBounds));
		
		initialBounds.offsetTo(0, 0);
		
		ViewHelper.animateBounds(viewBitmap, initialBounds);
	}

	@Override
	public List<ShowcaseElement> getShowcasedViews() {
		List<ShowcaseElement> elements = new ArrayList<ShowcaseElement>();
		
		elements.add(new ShowcaseElement(gridView.getChildAt(0), getString(R.string.available_fragment_grid_element_title), getString(R.string.available_fragment_grid_element_description)));
		
		return elements;
	}

}
