package com.mrk.nfcmanager.ui.fragments;

import java.util.ArrayList;
import java.util.List;

import android.app.Fragment;
import android.graphics.Color;
import android.nfc.NdefRecord;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.mrk.nfcmanager.R;
import com.mrk.nfcmanager.ext.floatingactionbutton.FloatingActionButton;
import com.mrk.nfcmanager.ext.ripplebackground.RippleBackground;
import com.mrk.nfcmanager.models.NFCRecord;
import com.mrk.nfcmanager.nfc.NFCStatus;
import com.mrk.nfcmanager.ui.HostScreen;
import com.mrk.nfcmanager.ui.delegates.HostScreenDelegateActions.Actions;
import com.mrk.nfcmanager.ui.delegates.ShowcaseElement;
import com.mrk.nfcmanager.ui.fragments.interfaces.FragmentNFCReadInterface;
import com.mrk.nfcmanager.ui.fragments.interfaces.FragmentNFCWriteInterface;
import com.mrk.nfcmanager.ui.fragments.interfaces.FragmentShowcaseInterface;
import com.mrk.nfcmanager.viewmodels.RecordViewModel;
import com.mrk.nfcmanager.viewmodels.TagViewModel;

public class MainFragment extends Fragment implements FragmentNFCReadInterface, FragmentNFCWriteInterface, OnClickListener, FragmentShowcaseInterface {
	
	private HostScreen hostActivity;
	
	private TextView title;
	private Button prepareButton;
	private Button eraseButton;
	private Button copyButton;
//	private Button lockButton;
	
	private RippleBackground rippleBackground;
	
	private FloatingActionButton cancelActionButton;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.main_fragment, container, false);
		
		title = (TextView) view.findViewById(R.id.wait_for_tag_title);
		prepareButton = (Button) view.findViewById(R.id.prepare_button);
		eraseButton = (Button) view.findViewById(R.id.erase_button);
		copyButton = (Button) view.findViewById(R.id.copy_button);
//		lockButton = (Button) view.findViewById(R.id.lock_button);
		
		prepareButton.setOnClickListener(this);
		eraseButton.setOnClickListener(this);
		copyButton.setOnClickListener(this);
//		lockButton.setOnClickListener(this);
		
		rippleBackground = (RippleBackground) view.findViewById(R.id.content);
	    
		cancelActionButton = new FloatingActionButton.Builder(getActivity()) 
	        .withDrawable(getResources().getDrawable(R.drawable.ic_floating_cancel_button)) 
	        .withButtonColor(Color.WHITE) 
	        .withGravity(Gravity.TOP | Gravity.END) 
	        .withMargins(0, 0, 16, 16) 
	        .create();
		cancelActionButton.setOnClickListener(this);
		
		return view;
	}
	
	@Override
	public void onStart() {
		super.onStart();
		this.hostActivity = (HostScreen)getActivity();
	}
	
	@Override
	public void onResume() {
		super.onResume();
		
		boolean nfcActive = this.hostActivity.getNfcStatus() == NFCStatus.OK;
		
		this.setRippleAnimation(nfcActive);
		this.eraseButton.setEnabled(nfcActive);
		this.copyButton.setEnabled(nfcActive);
		
		String titleString = getResources().getString(R.string.approach_message);
		
		if (!nfcActive) {
			titleString = getResources().getString(R.string.nfc_disabled_title);
		}
		
		title.setText(titleString);
	}
	
	@Override
	public void onStop() {
		this.setRippleAnimation(false);
		hideCancelButton();
		super.onStop();
	}
	
	@Override
	public void onNfcInfoReceived() {
		this.hostActivity.sendActionToDelegate(Actions.MAIN_READ.toString());
		this.hostActivity.doneWaitingForTag();
	}

	@Override
	public void writeNFC() {
		// we erased a tag
		hideCancelButton();
		this.hostActivity.sendActionToDelegate(Actions.MAIN_RESTART.toString());
	}
	
	@Override
	public void onClick(View view) {
		if (view.equals(cancelActionButton)) {
			hideCancelButton();
			this.hostActivity.sendActionToDelegate(Actions.MAIN_RESTART.toString());
		} else {
			switch (view.getId()) {
			case R.id.prepare_button:
				hostActivity.clearViewModel();
				hostActivity.sendActionToDelegate(Actions.MAIN_PREPARE.toString());
				break;
			case R.id.erase_button:
				prepareEmptyRecord();
				hostActivity.sendActionToDelegate(Actions.MAIN_ERASE.toString());
				break;
			case R.id.copy_button:
				hostActivity.sendActionToDelegate(Actions.MAIN_WAIT_FOR_COPY.toString());
				break;
//			case R.id.lock_button:
//				hostActivity.sendActionToDelegate(Actions.MAIN_LOCK.toString());
//				break;
			}
		}
	}
	
	public void setWaitingTitle(String title) {
		this.title.setText(title);
	}
	
	public void setRippleAnimation(final boolean animate) {		
		if (animate) {
			rippleBackground.startRippleAnimation();
		} else {
			rippleBackground.stopRippleAnimation();
		}
	}

	public void showCancelButton() {
		cancelActionButton.showFloatingActionButton();
		
		prepareButton.setEnabled(false);
		eraseButton.setEnabled(false);
		copyButton.setEnabled(false);
//		lockButton.setEnabled(false);
	}
	
	public void hideCancelButton() {
		cancelActionButton.hideFloatingActionButton();
		
		prepareButton.setEnabled(true);
		eraseButton.setEnabled(true);
		copyButton.setEnabled(true);
//		lockButton.setEnabled(true);
	}
	
	private void prepareEmptyRecord() {
		TagViewModel tagViewModel = new TagViewModel(getActivity());
		RecordViewModel vm = new RecordViewModel(getActivity());
		NFCRecord record = new NFCRecord();
		
		record.setTnf(NdefRecord.TNF_EMPTY);
		vm.loadFromData(record);
		tagViewModel.addRecordToList(vm);
		
		hostActivity.updateViewModel(tagViewModel);
	}

	@Override
	public List<ShowcaseElement> getShowcasedViews() {
		List<ShowcaseElement> elements = new ArrayList<ShowcaseElement>();
		
		elements.add(new ShowcaseElement(prepareButton, getString(R.string.main_fragment_prepare_button_title), getString(R.string.main_fragment_prepare_button_description)));
		elements.add(new ShowcaseElement(eraseButton, getString(R.string.main_fragment_erase_button_title), getString(R.string.main_fragment_erase_button_description)));
		elements.add(new ShowcaseElement(copyButton, getString(R.string.main_fragment_copy_button_title), getString(R.string.main_fragment_copy_button_description)));
		
		return elements;
	}
}
