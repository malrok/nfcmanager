package com.mrk.nfcmanager.ui.fragments.interfaces;

import java.util.List;

import com.mrk.nfcmanager.ui.delegates.ShowcaseElement;

public interface FragmentShowcaseInterface {

	public List<ShowcaseElement> getShowcasedViews();
	
}
