package com.mrk.nfcmanager.ui.fragments.interfaces;


public interface FragmentNFCWriteInterface {

	public void writeNFC();
	
}
