package com.mrk.nfcmanager.ui.fragments.interfaces;


public interface FragmentNFCReadInterface {

	public void onNfcInfoReceived();
	
}
