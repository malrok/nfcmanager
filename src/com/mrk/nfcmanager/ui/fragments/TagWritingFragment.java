package com.mrk.nfcmanager.ui.fragments;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import android.app.Fragment;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.TextView;

import com.fasterxml.jackson.jr.ob.JSON;
import com.mrk.mrkframework.viewmodel.interfaces.IViewModel;
import com.mrk.nfcmanager.R;
import com.mrk.nfcmanager.ext.dynamicgrid.DynamicGridView;
import com.mrk.nfcmanager.ext.floatingactionbutton.FloatingActionButton;
import com.mrk.nfcmanager.models.NFCRecord;
import com.mrk.nfcmanager.ui.HostScreen;
import com.mrk.nfcmanager.ui.adapters.DragDropGridAdapter;
import com.mrk.nfcmanager.ui.delegates.HostScreenDelegateActions.Actions;
import com.mrk.nfcmanager.ui.delegates.ShowcaseElement;
import com.mrk.nfcmanager.ui.fragments.interfaces.FragmentNFCReadInterface;
import com.mrk.nfcmanager.ui.fragments.interfaces.FragmentNFCWriteInterface;
import com.mrk.nfcmanager.ui.fragments.interfaces.FragmentShowcaseInterface;
import com.mrk.nfcmanager.ui.views.ActionView;
import com.mrk.nfcmanager.viewmodels.TagViewModel;

public class TagWritingFragment extends Fragment implements OnClickListener, FragmentNFCReadInterface, FragmentNFCWriteInterface, FragmentShowcaseInterface {

	private String originalData;
	private HostScreen hostActivity;
	
	private DynamicGridView gridview;
	private TextView actionsSize;
	
	/** floating add button */
	private FloatingActionButton addRecordButton;
	
	/** viewmodels */
	public TagViewModel tagViewModel;
	public IViewModel<NFCRecord> draggedVM;
	
	private DragDropGridAdapter gridAdapter;
	
	private boolean justOpened = false;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.tag_writing_fragment, container, false);
		
		actionsSize = (TextView) view.findViewById(R.id.actions_size);
		gridview = (DynamicGridView) view.findViewById(R.id.actions_gridview);
		
		gridview.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
            	draggedVM = tagViewModel.getRecordsList().getListData().get(position);
            	gridview.startEditMode(position);
                return true;
            }
        });
		
		gridview.setOnDropListener(new DynamicGridView.OnDropListener() {
			@Override
			public void onActionDrop(boolean droppedToBin) {				
				if (droppedToBin) {
			    	tagViewModel.getRecordsList().removeVM(draggedVM);
			    	gridAdapter = new DragDropGridAdapter(getActivity(), tagViewModel.getRecordsList().getListData(), 4);
			        gridview.setAdapter(gridAdapter);
			    	draggedVM = null;
			    	
			    	updateUsedSize();
			    }
				
				gridview.stopEditMode();
				computeWriteButton();
			}
		});
		
		gridview.setOnDragListener(new DynamicGridView.OnDragListener() {
            @Override
            public void onDragStarted(int position) {
            	
            }

            @Override
            public void onDragPositionsChanged(int oldPosition, int newPosition) {
            	Collections.swap(tagViewModel.getRecordsList().getListData(), oldPosition, newPosition);
            	tagViewModel.updateRecordsRank();
            }
        });
		
		gridview.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				View item = gridview.getChildAt(position);
				
				if (item instanceof ActionView) {
					((ActionView) item).onClick();
					computeWriteButton();
				}
			}
		});
		
		addRecordButton = new FloatingActionButton.Builder(getActivity()) 
	        .withDrawable(getResources().getDrawable(R.drawable.ic_floating_button_add)) 
	        .withButtonColor(Color.WHITE) 
	        .withGravity(Gravity.BOTTOM | Gravity.END) 
	        .withMargins(0, 0, 16, 16) 
	        .create();
		
		addRecordButton.setOnClickListener(this);
		
		return view;
	}
	
	@Override
	public void onStart() {
		super.onStart();
		this.hostActivity = (HostScreen)getActivity();
		
		if (this.justOpened) {
			// we just opened the fragment for preparing, nothing in it,n so lets head directly to available actions
			this.justOpened = false;
			this.hostActivity.sendActionToDelegate(Actions.DETAILS_ADD.toString());
		} else {
			addRecordButton.showFloatingActionButton();
		}
	}
	
	@Override
	public void onResume() {
		super.onResume();
		applyStatus();
	}
	
	@Override
	public void onStop() {
		super.onStop();
		addRecordButton.hideFloatingActionButton();
		this.hostActivity.setWriteVisibility(false);
	}
	
	@Override
	public void onClick(View view) {
		if (view.equals(addRecordButton)) {
			this.hostActivity.sendActionToDelegate(Actions.DETAILS_ADD.toString());
		}
	}
	
	private void fillRecordsGrid() {
		gridAdapter = new DragDropGridAdapter(hostActivity, tagViewModel.getRecordsList().getListData(false), 4);
		gridview.setAdapter(gridAdapter);
		
		updateUsedSize();		
	}

	public void updateUsedSize() {
		if (this.hostActivity.getNdefMessage() != null) {
			actionsSize.setVisibility(View.VISIBLE);
//			actionsSize.setText("Used size will be " + this.hostActivity.getNdefMessage().getByteArrayLength() + " bytes");
			String sizeText = this.hostActivity.getResources().getString(R.string.used_size);
	        sizeText = sizeText.replaceAll("@", String.valueOf(this.hostActivity.getNdefMessage() == null ? 0 : this.hostActivity.getNdefMessage().getByteArrayLength()));
	        
	        actionsSize.setText(sizeText);
		} else {
			actionsSize.setVisibility(View.GONE);
		}
	}
	
	@Override
	public void onNfcInfoReceived() {
		tagViewModel = hostActivity.getViewModel();
		
		fillRecordsGrid();
	}
	
	@Override
	public void writeNFC() {
		this.hostActivity.doneWaitingForTag();
	}

	@Override
	public List<ShowcaseElement> getShowcasedViews() {
		List<ShowcaseElement> elements = new ArrayList<ShowcaseElement>();
		
		elements.add(new ShowcaseElement(addRecordButton, getString(R.string.writing_fragment_add_button_title), getString(R.string.writing_fragment_add_button_description)));
		if (tagViewModel.getRecordsList().getListData(false).size() > 0) {
			elements.add(new ShowcaseElement(gridview.getChildAt(0), getString(R.string.writing_fragment_grid_element_title), getString(R.string.writing_fragment_grid_element_description)));
			elements.add(new ShowcaseElement(R.id.write, getString(R.string.writing_fragment_write_title), getString(R.string.writing_fragment_write_description)));
		}
		
		return elements;
	}
	
	public void setOriginalData(String originalData) {
		this.originalData = originalData;
		this.justOpened = this.originalData == null;
	}
	
	private void applyStatus() {
		tagViewModel = this.hostActivity.getViewModel();
		
		computeWriteButton();		
		fillRecordsGrid();
	}

	private void computeWriteButton() {
		String currentData = null;
		
		try {
			currentData = JSON.std.asString(((HostScreen)getActivity()).getViewModel());
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		if ((originalData == null && ((HostScreen)getActivity()).getViewModel().getRecordsList().getData().size() > 0) || (originalData != null && !originalData.equals(currentData))) {
			this.hostActivity.setWriteVisibility(true);
		} else {
			this.hostActivity.updateViewModel(tagViewModel);
			this.hostActivity.setWriteVisibility(false);
		}
	}
}
