package com.mrk.nfcmanager.ui.fragments;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import android.app.Fragment;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.TextView;

import com.fasterxml.jackson.jr.ob.JSON;
import com.mrk.mrkframework.viewmodel.interfaces.IViewModel;
import com.mrk.nfcmanager.R;
import com.mrk.nfcmanager.ext.dynamicgrid.DynamicGridView;
import com.mrk.nfcmanager.ext.floatingactionbutton.FloatingActionButton;
import com.mrk.nfcmanager.models.NFCRecord;
import com.mrk.nfcmanager.ui.HostScreen;
import com.mrk.nfcmanager.ui.adapters.DragDropGridAdapter;
import com.mrk.nfcmanager.ui.delegates.HostScreenDelegateActions.Actions;
import com.mrk.nfcmanager.ui.delegates.ShowcaseElement;
import com.mrk.nfcmanager.ui.dialogs.TagDetailDialog;
import com.mrk.nfcmanager.ui.fragments.interfaces.FragmentNFCReadInterface;
import com.mrk.nfcmanager.ui.fragments.interfaces.FragmentNFCWriteInterface;
import com.mrk.nfcmanager.ui.fragments.interfaces.FragmentShowcaseInterface;
import com.mrk.nfcmanager.ui.views.ActionView;
import com.mrk.nfcmanager.viewmodels.TagViewModel;

public class TagReadingFragment extends Fragment implements OnClickListener, FragmentNFCReadInterface, FragmentNFCWriteInterface, FragmentShowcaseInterface {

	private HostScreen hostActivity;
	
	/* views */
	private DynamicGridView gridview;
	private TextView tagId;
	private TextView tagTechs;
	private TextView tagSize;
	private TextView tagType;
	private TextView tagReadonly;
	
	/* viewmodels */
	public TagViewModel tagViewModel;
	public IViewModel<NFCRecord> draggedVM;
	
	/* adapter */
	private DragDropGridAdapter gridAdapter;

	/* 
	 * this is used to store the current data as a string
	 * when the value changes, the write button appears
	 */
	private String originalData;
	
	/** floating buttons */
	private FloatingActionButton addRecordButton;
	private FloatingActionButton tagInfoButton;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.tag_reading_fragment, container, false);
		
		gridview = (DynamicGridView) view.findViewById(R.id.actions_gridview);
		
		tagId = (TextView) view.findViewById(R.id.tag_id);
		tagTechs = (TextView) view.findViewById(R.id.tag_techs);
		tagSize = (TextView) view.findViewById(R.id.tag_size);
		tagType = (TextView) view.findViewById(R.id.tag_type);
		tagReadonly = (TextView) view.findViewById(R.id.tag_readonly);
		
		gridview.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
            	draggedVM = tagViewModel.getRecordsList().getListData().get(position);
            	gridview.startEditMode(position);
                return true;
            }
        });
		
		gridview.setOnDropListener(new DynamicGridView.OnDropListener() {
			@Override
			public void onActionDrop(boolean droppedToBin) {				
				if (droppedToBin) {
			    	tagViewModel.getRecordsList().removeVM(draggedVM);
			    	gridAdapter = new DragDropGridAdapter(getActivity(), tagViewModel.getRecordsList().getListData(), 4);
			        gridview.setAdapter(gridAdapter);
			    	draggedVM = null;
			    }
				
				gridview.stopEditMode();
				computeWriteButton();
			}
		});
		
		gridview.setOnDragListener(new DynamicGridView.OnDragListener() {
            @Override
            public void onDragStarted(int position) {
            	
            }

            @Override
            public void onDragPositionsChanged(int oldPosition, int newPosition) {
            	Collections.swap(tagViewModel.getRecordsList().getListData(), oldPosition, newPosition);
            	tagViewModel.updateRecordsRank();
            }
        });
		
		gridview.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				View item = gridview.getChildAt(position);
				
				if (item instanceof ActionView) {
					((ActionView) item).onClick();
					computeWriteButton();
				}
			}
		});
		
		tagInfoButton = new FloatingActionButton.Builder(getActivity()) 
	        .withDrawable(getResources().getDrawable(R.drawable.ic_info)) 
	        .withButtonColor(Color.WHITE) 
	        .withGravity(Gravity.BOTTOM | Gravity.LEFT) 
	        .withMargins(16, 0, 0, 16) 
	        .create();
		
		tagInfoButton.setOnClickListener(this);
		
		addRecordButton = new FloatingActionButton.Builder(getActivity()) 
	        .withDrawable(getResources().getDrawable(R.drawable.ic_floating_button_add)) 
	        .withButtonColor(Color.WHITE) 
	        .withGravity(Gravity.BOTTOM | Gravity.RIGHT) 
	        .withMargins(0, 0, 16, 16) 
	        .create();
		
		addRecordButton.setOnClickListener(this);
		
		return view;
	}
	
	@Override
	public void onStart() {
		super.onStart();
		
		this.hostActivity = (HostScreen)getActivity();
		
		addRecordButton.showFloatingActionButton();
	}
	
	@Override
	public void onResume() {
		super.onResume();
		applyStatus();
		
		if (tagViewModel.getRecordsList().getListData(false).size() > 0) {
        	tagInfoButton.showFloatingActionButton();
        }
	}
	
	@Override
	public void onStop() {
		super.onStop();
		
		this.hostActivity.setWriteVisibility(false);
		
		addRecordButton.hideFloatingActionButton();
		tagInfoButton.hideFloatingActionButton();
	}
	
	private void fillTagDetails() {
		tagId.setText(tagViewModel.getTagId());
		tagTechs.setText(tagViewModel.getTagTechs());
		
		String sizeText = tagViewModel.getTagSize();
		
		if (this.hostActivity.getNdefMessage() != null) {
			sizeText = this.hostActivity.getNdefMessage().getByteArrayLength() + "/" + tagViewModel.getTagSize();
		}
		
		tagSize.setText(sizeText);
		tagType.setText(tagViewModel.getTagType());
		tagReadonly.setText(tagViewModel.getTagReadonly());
	}
	
	private void fillRecordsGrid() {
		gridAdapter = new DragDropGridAdapter(hostActivity, tagViewModel.getRecordsList().getListData(false), 4);
		gridview.setAdapter(gridAdapter);
	}

	@Override
	public void onNfcInfoReceived() {
		tagViewModel = hostActivity.getViewModel();
		
		fillTagDetails();
		fillRecordsGrid();
	}

	@Override
	public void onClick(View view) {
		if (view.equals(addRecordButton)) {
			this.hostActivity.sendActionToDelegate(Actions.DETAILS_ADD.toString());
		} else if (view.equals(tagInfoButton)) {
			TagDetailDialog dialog = new TagDetailDialog(getActivity(), tagViewModel);
			dialog.show();
		}
	}
	
	@Override
	public void writeNFC() {
		// nothing to do here
	}

	public void setOriginalData(String originalData) {
		this.originalData = originalData;
	}
	
	private void applyStatus() {
		tagViewModel = this.hostActivity.getViewModel();
		
		computeWriteButton();
		fillTagDetails();
		fillRecordsGrid();
	}

	private void computeWriteButton() {
		String currentData = null;
		
		try {
			currentData = JSON.std.asString(((HostScreen)getActivity()).getViewModel());
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		if ((originalData == null && ((HostScreen)getActivity()).getViewModel().getRecordsList().getData().size() > 0) || (originalData != null && !originalData.equals(currentData))) {
			this.hostActivity.setWriteVisibility(true);
		} else {
			this.hostActivity.updateViewModel(tagViewModel);
			this.hostActivity.setWriteVisibility(false);
		}
	}

	@Override
	public List<ShowcaseElement> getShowcasedViews() {
		List<ShowcaseElement> elements = new ArrayList<ShowcaseElement>();
		
		elements.add(new ShowcaseElement(addRecordButton, getString(R.string.tag_reading_fragment_add_button_title), getString(R.string.tag_reading_fragment_add_button_description)));
		elements.add(new ShowcaseElement(tagInfoButton, getString(R.string.tag_reading_fragment_info_button_title), getString(R.string.tag_reading_fragment_info_button_description)));
		elements.add(new ShowcaseElement(tagId, getString(R.string.tag_reading_fragment_tag_detail_title), getString(R.string.tag_reading_fragment_tag_detail_description)));
		
		if (tagViewModel.getRecordsList().getListData(false).size() > 0) {
			elements.add(new ShowcaseElement(gridview.getChildAt(0), getString(R.string.tag_reading_fragment_tag_content_title), getString(R.string.tag_reading_fragment_tag_content_description)));
		}
		
		return elements;
	}

}
