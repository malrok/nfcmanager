package com.mrk.nfcmanager.ui.fragments;

import android.os.Bundle;
import android.preference.PreferenceFragment;

import com.mrk.nfcmanager.R;

public class PreferencesFragment extends PreferenceFragment { // implements OnPreferenceClickListener {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.preferences);
	}
	
//	@Override
//	public boolean onPreferenceClick(Preference preference) {
//		// TODO Auto-generated method stub
//		return false;
//	}

}
