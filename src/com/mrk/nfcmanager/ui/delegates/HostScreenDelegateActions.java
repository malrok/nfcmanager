package com.mrk.nfcmanager.ui.delegates;

import com.mrk.mrkframework.ui.fragmentdelegate.interfaces.DelegateActions;

public class HostScreenDelegateActions implements DelegateActions {

	public static enum Actions {
		/** activity */
		ACTIVITY_RESUME,
		
		/** MAIN fragment */
		MAIN_READ,
		MAIN_PREPARE,
		MAIN_ERASE,
		MAIN_WAIT_FOR_COPY,
		MAIN_COPY,
		MAIN_LOCK,
		MAIN_RESTART,
		
		/** DETAILS fragment */
		DETAILS_ADD,
		DETAILS_WRITE,
		
		/** AVAILABLE actions fragment */
		AVAILABLE_ADD,
		
		/** preferences */
		PREFERENCES
	}
	
	private String currentAction;
	
	@Override
	public void setAction(String action) {
		currentAction = action;
	}

	@Override
	public String getAction() {
		return currentAction;
	}
	
}
