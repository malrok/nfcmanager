package com.mrk.nfcmanager.ui.delegates;

import java.io.IOException;

import android.app.Activity;
import android.app.Fragment;
import android.util.Log;

import com.fasterxml.jackson.jr.ob.JSON;
import com.fasterxml.jackson.jr.ob.JSONObjectException;
import com.mrk.mrkframework.ui.fragmentdelegate.AbstractFragmentActivityDelegate;
import com.mrk.mrkframework.ui.fragmentdelegate.WrongFragmentException;
import com.mrk.nfcmanager.R;
import com.mrk.nfcmanager.ui.HostScreen;
import com.mrk.nfcmanager.ui.delegates.HostScreenDelegateActions.Actions;
import com.mrk.nfcmanager.ui.enums.WaitForTag;
import com.mrk.nfcmanager.ui.fragments.AvailableActionsFragment;
import com.mrk.nfcmanager.ui.fragments.CopyTagFragment;
import com.mrk.nfcmanager.ui.fragments.MainFragment;
import com.mrk.nfcmanager.ui.fragments.PreferencesFragment;
import com.mrk.nfcmanager.ui.fragments.TagReadingFragment;
import com.mrk.nfcmanager.ui.fragments.TagWritingFragment;
import com.mrk.nfcmanager.ui.fragments.interfaces.FragmentShowcaseInterface;

public class HostScreenDelegate extends AbstractFragmentActivityDelegate {

	private String previousFragment;
	
	public HostScreenDelegate(Activity activity, int fragmentContainer) {
		super(activity, fragmentContainer);
	}

	@Override
	public void onBackPressed() {
		try {
			if (currentFragment.equals(getFragmentFromClassName(MainFragment.class.getName()))) {
				getActivity().finish();
			}
			setPreviousFragment();
		} catch (WrongFragmentException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void setPreviousFragment() {
		try {
			if (currentFragment.equals(getFragmentFromClassName(TagWritingFragment.class.getName()))
				|| currentFragment.equals(getFragmentFromClassName(TagReadingFragment.class.getName()))
				|| currentFragment.equals(getFragmentFromClassName(CopyTagFragment.class.getName()))) {
				changeFragment(getFragmentFromClassName(MainFragment.class.getName()));
				((HostScreen)getActivity()).waitForTag(WaitForTag.READ, false);
				((HostScreen)getActivity()).toggleMenuItems(false, true);
			} else if (currentFragment.equals(getFragmentFromClassName(AvailableActionsFragment.class.getName()))) {
				changeFragment(getFragmentFromClassName(TagWritingFragment.class.getName()));
				((HostScreen)getActivity()).toggleMenuItems(true, true);
			} else if (currentFragment.equals(getFragmentFromClassName(PreferencesFragment.class.getName()))) {
				changeFragment(getFragmentFromClassName(previousFragment));
				((HostScreen)getActivity()).toggleMenuItems(false, true);
			}
		} catch (WrongFragmentException e) {
			e.printStackTrace();
		}
		
		((HostScreen)getActivity()).setHelpVisibility(currentFragment instanceof FragmentShowcaseInterface);
		((HostScreen)getActivity()).setPreferencesVisibility(currentFragment instanceof PreferencesFragment);
	}
	
	@Override
	public void processAction(String action) {
		Fragment fragment = null;
		boolean addToBackStack = true;
		
		((HostScreen)getActivity()).doneWaitingForTag();
		
		try {
			switch(Actions.valueOf(action)) {
			case ACTIVITY_RESUME:
				fragment = getFragmentFromClassName(MainFragment.class.getName());
				((HostScreen)getActivity()).waitForTag(WaitForTag.READ, false);
				((HostScreen)getActivity()).toggleMenuItems(false, true);
				break;
			case MAIN_READ:
				fragment = getFragmentFromClassName(TagReadingFragment.class.getName());
				String vmData = JSON.std.asString(((HostScreen)getActivity()).getViewModel());
				((TagReadingFragment)fragment).setOriginalData(vmData);
				((HostScreen)getActivity()).toggleMenuItems(true, true);
				break;
			case MAIN_PREPARE:
				fragment = getFragmentFromClassName(TagWritingFragment.class.getName());
				((TagWritingFragment)fragment).setOriginalData(null);
				((HostScreen)getActivity()).toggleMenuItems(true, true);
				break;
			case MAIN_ERASE:
				((HostScreen)getActivity()).getNdefMessage();
				((HostScreen)getActivity()).waitForTagWithTitle(getActivity().getResources().getString(R.string.approach_erase_message), WaitForTag.DELETE, false);
				((MainFragment)((HostScreen)getActivity()).getCurrentFragment()).showCancelButton();
				break;
			case MAIN_WAIT_FOR_COPY:
				((HostScreen)getActivity()).waitForTagWithTitle(getActivity().getResources().getString(R.string.approach_copy_message), WaitForTag.COPY, false);
				((MainFragment)((HostScreen)getActivity()).getCurrentFragment()).showCancelButton();
				break;
			case MAIN_COPY:
				fragment = getFragmentFromClassName(CopyTagFragment.class.getName());
				((HostScreen)getActivity()).toggleMenuItems(true, true);
				break;
			case MAIN_LOCK:
				break;
			case MAIN_RESTART:
				((MainFragment) currentFragment).setRippleAnimation(true);
				((HostScreen)getActivity()).waitForTag(WaitForTag.READ, false);
				((HostScreen)getActivity()).setHelpVisibility(currentFragment instanceof FragmentShowcaseInterface);
				return;
			case DETAILS_ADD:
				fragment = getFragmentFromClassName(AvailableActionsFragment.class.getName());
				((HostScreen)getActivity()).toggleMenuItems(true, true);
				break;
			case DETAILS_WRITE:
				break;
			case AVAILABLE_ADD:
				break;
			case PREFERENCES:
				previousFragment = currentFragment.getClass().getName();
				fragment = getFragmentFromClassName(PreferencesFragment.class.getName());
				((HostScreen)getActivity()).toggleMenuItems(true, false);
				break;
			}
		} catch (WrongFragmentException e) {
			Log.d("HostScreenDelegate", "Wrong enum");
		} catch (JSONObjectException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		((HostScreen)getActivity()).setHelpVisibility(fragment instanceof FragmentShowcaseInterface);
		((HostScreen)getActivity()).setPreferencesVisibility(fragment instanceof PreferencesFragment);
		
		changeFragment(fragment, addToBackStack);
	}
	
}
