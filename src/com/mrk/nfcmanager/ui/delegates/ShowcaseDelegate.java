package com.mrk.nfcmanager.ui.delegates;

import java.util.List;

import android.app.Activity;
import android.app.Fragment;

import com.mrk.nfcmanager.R;
import com.mrk.nfcmanager.ext.showcaseview.OnShowcaseEventListener;
import com.mrk.nfcmanager.ext.showcaseview.ShowcaseView;
import com.mrk.nfcmanager.ext.showcaseview.targets.ActionItemTarget;
import com.mrk.nfcmanager.ext.showcaseview.targets.ViewTarget;
import com.mrk.nfcmanager.ui.fragments.interfaces.FragmentShowcaseInterface;


public class ShowcaseDelegate implements OnShowcaseEventListener {

	private static ShowcaseDelegate instance;
	
	private Activity activity;
	
	private ShowcaseView showcase;
	private List<ShowcaseElement> fragmentsHelpList;
	private int currentShowcase;
	
	public static ShowcaseDelegate getInstance() {
		if (instance == null) {
			instance = new ShowcaseDelegate();
		}
		return instance;
	}
	
	private ShowcaseDelegate() {
		// nothing to do
	}
	
	public boolean show(Fragment fragment) {
		if (fragment instanceof FragmentShowcaseInterface) {
			showcaseForFragment(fragment);
			return true;
		}
		
		return false;
	}

	private void showcaseForFragment(Fragment fragment) {
		activity = fragment.getActivity();
		fragmentsHelpList = ((FragmentShowcaseInterface)fragment).getShowcasedViews();
		currentShowcase = 0;
		
		showShowcase();
	}

	private void showShowcase() {
		if (currentShowcase >= fragmentsHelpList.size()) {
			return;
		}
		
		ShowcaseElement element = fragmentsHelpList.get(currentShowcase);
		
		if (element.fromView()) {
			ViewTarget target = new ViewTarget(element.getView());
			
			showcase = new ShowcaseView.Builder(activity)
			    .setTarget(target)
			    .setContentTitle(element.getTitle())
			    .setContentText(element.getText())
			    .setStyle(R.style.ShowcaseView2)
			    .hideOnTouchOutside()
			    .build();
		} else {
			ActionItemTarget target = new ActionItemTarget(activity, element.getMenuId());
			
			showcase = new ShowcaseView.Builder(activity)
			    .setTarget(target)
			    .setContentTitle(element.getTitle())
			    .setContentText(element.getText())
			    .setStyle(R.style.ShowcaseView2)
			    .hideOnTouchOutside()
			    .build();
		}
		
		showcase.setOnShowcaseEventListener(this);
	}
	
	@Override
	public void onShowcaseViewHide(ShowcaseView showcaseView) {
		// nothing to do
	}

	@Override
	public void onShowcaseViewDidHide(ShowcaseView showcaseView) {
		currentShowcase++;
		showShowcase();
	}

	@Override
	public void onShowcaseViewShow(ShowcaseView showcaseView) {
		// nothing to do
	}
	
}
