package com.mrk.nfcmanager.ui.delegates;

import android.view.View;


public class ShowcaseElement {

	private View view;
	private int menuId;
	private String title;
	private String text;

	public ShowcaseElement(View view, String title, String text) {
		this.view = view;
		this.title = title;
		this.text = text;
	}
	
	public ShowcaseElement(int menuId, String title, String text) {
		this.menuId = menuId;
		this.title = title;
		this.text = text;
	}
	
	public boolean fromView() {
		return this.view != null;
	}
	
	public View getView() {
		return view;
	}
	
	public int getMenuId() {
		return menuId;
	}
	
	public String getTitle() {
		return title;
	}
	
	public String getText() {
		return text;
	}
}
