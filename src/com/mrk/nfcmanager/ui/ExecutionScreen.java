package com.mrk.nfcmanager.ui;

import android.app.Activity;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.CheckBox;
import android.widget.TextView;

import com.mrk.mrkframework.viewmodel.interfaces.IViewModel;
import com.mrk.nfcmanager.R;
import com.mrk.nfcmanager.dataloaders.InternalActionsDataLoader;
import com.mrk.nfcmanager.dataloaders.TagsDataLoader;
import com.mrk.nfcmanager.models.NFCRecord;
import com.mrk.nfcmanager.viewmodels.RecordViewModel;
import com.mrk.nfcmanager.viewmodels.TagViewModel;

public class ExecutionScreen extends Activity {

	private TagViewModel tagViewModel;
	private InternalActionsDataLoader actionsLoader;
	
	private SharedPreferences preferences;
	
	@Override
    protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		preferences = PreferenceManager.getDefaultSharedPreferences(this);
		
		tagViewModel = new TagViewModel(getApplicationContext());
		
		setContentView(R.layout.tag_execution_screen);
        
		TextView title = (TextView) findViewById(R.id.wait_title);
		title.setText(getResources().getString(R.string.applying_tag_commands));
		
		final CheckBox autoClose = (CheckBox) findViewById(R.id.auto_close);
		int id = Resources.getSystem().getIdentifier("btn_check_holo_light", "drawable", "android");
		autoClose.setButtonDrawable(id);
		autoClose.setChecked(preferences.getBoolean("autoclose_execute_activity", false));
		autoClose.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				preferences.edit().putBoolean("autoclose_execute_activity", autoClose.isChecked()).commit();
			}
		});
	}
	
	@Override
	public void onResume() {
		super.onResume();

		actionsLoader = new InternalActionsDataLoader();
		
		if (NfcAdapter.ACTION_NDEF_DISCOVERED.equals(getIntent().getAction()) ||
			NfcAdapter.ACTION_TAG_DISCOVERED.equals(getIntent().getAction())) {
			
			tagViewModel.clear();
			tagViewModel.loadFromData(new TagsDataLoader().getNFCTagFromIntent(getApplicationContext(), getIntent()));
			
			for (IViewModel<NFCRecord> viewModel : tagViewModel.getRecordsList().getListData()) {
				RecordViewModel vm = (RecordViewModel)viewModel;
				actionsLoader.getActionFromType(getApplicationContext(), vm.getData().getActionType()).execute(getApplicationContext(), vm.getData().getStringParams());
			}
			
			dismiss();
		}
	}

	private void dismiss() {
        if (preferences.getBoolean("autoclose_execute_activity", false)) {
        	this.finish();
        }
	}
}
