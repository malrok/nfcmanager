package com.mrk.nfcmanager.ui.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Window;
import android.widget.ListView;

import com.mrk.nfcmanager.R;
import com.mrk.nfcmanager.ui.adapters.PayloadsListAdapter;
import com.mrk.nfcmanager.viewmodels.TagViewModel;

public class TagDetailDialog extends Dialog {

	private ListView payloadsListView;

	private PayloadsListAdapter payloadsAdapter;

	private TagViewModel tagViewModel;

	public TagDetailDialog(Context context, TagViewModel tagViewModel) {
		super(context);
		this.tagViewModel = tagViewModel;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		this.requestWindowFeature(Window.FEATURE_NO_TITLE);

		setContentView(R.layout.tag_detail_dialog);

		payloadsListView = (ListView) findViewById(R.id.payloads_list);
		
		payloadsAdapter = new PayloadsListAdapter(getContext(), tagViewModel);
		payloadsListView.setAdapter(payloadsAdapter);
//		new Handler().postDelayed(new Runnable() {
//			@Override
//			public void run() {
//				ViewHelper.setListViewHeightBasedOnChildren(payloadsListView);
//			}
//		}, 300);
	}

}
