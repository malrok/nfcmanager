package com.mrk.nfcmanager.ui.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Window;
import android.widget.TextView;

import com.mrk.nfcmanager.R;
import com.mrk.nfcmanager.ext.ripplebackground.RippleBackground;

public class WaitDialog extends Dialog {

	private String dialogTitle;
	
	private TextView titleView;
	private RippleBackground rippleBackground;
	
	public WaitDialog(Context context, String title) {
		super(context);
		
		this.dialogTitle = title;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		this.requestWindowFeature(Window.FEATURE_NO_TITLE);

		setContentView(R.layout.wait_dialog);

		rippleBackground=(RippleBackground) findViewById(R.id.content);	    
		titleView = (TextView) findViewById(R.id.wait_title);
	}
	
//	@Override
//	public void dismiss() {
//		try {
//			rippleBackground.stopRippleAnimation();
//		} catch (IllegalStateException e) {
//			e.printStackTrace();
//		}
//		super.dismiss();
//	}
	
	@Override
	public void show() {
		super.show();
		
		rippleBackground.startRippleAnimation();
		titleView.setText(dialogTitle);
	}
	
	public void setTitle(String title) {
		this.dialogTitle = title;
	}
}
