package com.mrk.nfcmanager.ui;

import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.graphics.Color;
import android.nfc.NdefMessage;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.mrk.mrkframework.ui.AbstractMRKFragmentActivity;
import com.mrk.mrkframework.ui.fragmentdelegate.interfaces.FragmentActivityDelegate;
import com.mrk.nfcmanager.R;
import com.mrk.nfcmanager.dataloaders.TagsDataLoader;
import com.mrk.nfcmanager.ext.materialmenu.MaterialMenuDrawable.IconState;
import com.mrk.nfcmanager.ext.materialmenu.MaterialMenuIcon;
import com.mrk.nfcmanager.helpers.InstalledApplicationHelper;
import com.mrk.nfcmanager.models.NFCTag;
import com.mrk.nfcmanager.nfc.NFCHandler;
import com.mrk.nfcmanager.nfc.NFCHandler.HandlerStatus;
import com.mrk.nfcmanager.nfc.NFCStatus;
import com.mrk.nfcmanager.nfc.NfcWriteCallback;
import com.mrk.nfcmanager.nfc.factory.NdefMessageFactory;
import com.mrk.nfcmanager.ui.delegates.HostScreenDelegate;
import com.mrk.nfcmanager.ui.delegates.HostScreenDelegateActions.Actions;
import com.mrk.nfcmanager.ui.delegates.ShowcaseDelegate;
import com.mrk.nfcmanager.ui.enums.WaitForTag;
import com.mrk.nfcmanager.ui.fragments.AvailableActionsFragment;
import com.mrk.nfcmanager.ui.fragments.CopyTagFragment;
import com.mrk.nfcmanager.ui.fragments.MainFragment;
import com.mrk.nfcmanager.ui.fragments.PreferencesFragment;
import com.mrk.nfcmanager.ui.fragments.TagReadingFragment;
import com.mrk.nfcmanager.ui.fragments.TagWritingFragment;
import com.mrk.nfcmanager.ui.fragments.interfaces.FragmentNFCReadInterface;
import com.mrk.nfcmanager.ui.fragments.interfaces.FragmentNFCWriteInterface;
import com.mrk.nfcmanager.viewmodels.TagViewModel;

public class HostScreen extends AbstractMRKFragmentActivity implements OnDismissListener {
	
	private TagViewModel tagViewModel;
	
	/** NFC Handler */
	private NFCHandler nfcHandler;
	
	/** menu items */
	private MenuItem writeItem;
	private MenuItem helpItem;
	private MenuItem preferencesItem;
	private MaterialMenuIcon materialMenu;
	
	private boolean menuCreated = false, helpVisible = false;
	private NdefMessage ndefMessage = null;
	
	@Override
    protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.host_screen);

        materialMenu = new MaterialMenuIcon(this, Color.WHITE);
        
        tagViewModel = new TagViewModel(getApplicationContext());
        
        nfcHandler = new NFCHandler(this);
        nfcHandler.setDialogDismissInterface(this);
        
        InstalledApplicationHelper.getInstance(getApplicationContext()); // triggers application discovery
	}
	
	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
	    materialMenu.syncState(savedInstanceState);
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
	    materialMenu.onSaveInstanceState(outState);
	}
	
	@Override
	public void initializeFragments() {
		addFragmentChild(new MainFragment());
		addFragmentChild(new TagWritingFragment());
		addFragmentChild(new TagReadingFragment());
		addFragmentChild(new AvailableActionsFragment());
		addFragmentChild(new CopyTagFragment());
		addFragmentChild(new PreferencesFragment());
	}

	@Override
	public FragmentActivityDelegate setDelegate() {
		return new HostScreenDelegate(this, R.id.fragment_container);
	}
	
	@Override
	public void onResume() {
		super.onResume();

		if (getCurrentFragment() == null) {	
			sendActionToDelegate(Actions.ACTIVITY_RESUME.toString());
		}
		
		checkNfc();
		
		PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, this.getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
		nfcHandler.enableNdefExchangeMode(pendingIntent);
	}

	private void checkNfc() {
		switch (nfcHandler.checkNfc()) {
	        case NFCUnable:
	            Toast.makeText(this, "NFC is not available", Toast.LENGTH_SHORT).show();
	            finish();
	            break;
	        case NFCDisabled:
	        	new AlertDialog.Builder(this)
					.setIcon(android.R.drawable.ic_dialog_alert)
					.setTitle(R.string.nfc_disabled_title)
					.setMessage(R.string.nfc_disabled)
					.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							startActivity(new Intent(android.provider.Settings.ACTION_WIRELESS_SETTINGS));
						}
					})
					.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
						}
					})
					.show();
	        	
	        	break;
	        default:
	        	break;
        }
	}
	
	@Override
	public void onPause() {
		super.onPause();
		nfcHandler.disableNdefExchangeMode();
	}
	
	@Override
    public void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		
		String intentAction = intent.getAction();
		if (NfcAdapter.ACTION_NDEF_DISCOVERED.equals(intentAction) ||
			NfcAdapter.ACTION_TAG_DISCOVERED.equals(intentAction)) {
			
			if (nfcHandler.getHandlerStatus() != HandlerStatus.PENDING) {
				NFCTag tag = new TagsDataLoader().getNFCTagFromIntent(getApplicationContext(), intent);
				
				if (tag != null) {
					if (nfcHandler.getHandlerStatus() == HandlerStatus.WAITING_TO_READ && getCurrentFragment() instanceof FragmentNFCReadInterface) {
						tagViewModel.clear();
						tagViewModel.loadFromData(tag);
						nfcHandler.doneReading();
						((FragmentNFCReadInterface) getCurrentFragment()).onNfcInfoReceived();
					} else if (nfcHandler.getHandlerStatus() == HandlerStatus.WAITING_FOR_COPY) {
						tagViewModel.clear();
						tagViewModel.loadFromData(tag);
						nfcHandler.doneReading();
						sendActionToDelegate(Actions.MAIN_COPY.toString());
					} else if (nfcHandler.getHandlerStatus() == HandlerStatus.WAITING_TO_WRITE && getCurrentFragment() instanceof FragmentNFCWriteInterface) {
						if (tag.getMaxSize() < this.ndefMessage.getByteArrayLength()) {
							Toast.makeText(getApplicationContext(), getResources().getString(R.string.tag_too_small), Toast.LENGTH_LONG).show();
							nfcHandler.doneReading();
						} else {
							nfcHandler.write(this, intent, new NfcWriteCallback() {
								@Override
								public void doAfterWrite() {
									((FragmentNFCWriteInterface) getCurrentFragment()).writeNFC();
								}
							});
						}
					} else if (nfcHandler.getHandlerStatus() == HandlerStatus.WAITING_TO_DELETE) {
						nfcHandler.write(this, intent, new NfcWriteCallback() {
							@Override
							public void doAfterWrite() {
								if (getCurrentFragment() instanceof FragmentNFCWriteInterface) {
									((FragmentNFCWriteInterface) getCurrentFragment()).writeNFC();
								}
							}
						});
					}
				}
			}
		}
    }
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu, menu);

		writeItem = menu.findItem(R.id.write);
		writeItem.setVisible(false);
		
		helpItem = menu.findItem(R.id.help);
		helpItem.setVisible(helpVisible);
		
		preferencesItem = menu.findItem(R.id.preferences);
		
		menuCreated = true;
		
		return true;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case android.R.id.home:
				setPreviousFragment();
				break;
			case R.id.write:
				if (nfcHandler.checkNfc() == NFCStatus.OK) {
					waitForTag(WaitForTag.WRITE, true);
				} else {
					checkNfc();
				}
				break;
			case R.id.help:
				ShowcaseDelegate.getInstance().show(this.getCurrentFragment());
				break;
			case R.id.preferences:
				this.sendActionToDelegate(Actions.PREFERENCES.toString());
				break;
		}
		return true;
	}
	
	public TagViewModel getViewModel() {
		return tagViewModel;
	}
	
	public NdefMessage getNdefMessage() {
		this.ndefMessage = new NdefMessageFactory().createFromViewModel(tagViewModel);
		return this.ndefMessage;
	}
	
	public void updateViewModel(TagViewModel vm) {
		this.tagViewModel = vm;
	}
	
	public void clearViewModel() {
		this.tagViewModel.clear();
		this.ndefMessage = null;
	}
	
	public void setWriteVisibility(boolean visible) {
		if (menuCreated && nfcHandler.checkNfc() == NFCStatus.OK) {
			writeItem.setVisible(visible);
		}
	}

	public View getWriteMenuItem() {
		return writeItem.getActionView();
	}
	
	public void setHelpVisibility(boolean visible) {
		helpVisible = visible;
		if (menuCreated) {
			helpItem.setVisible(helpVisible);
		}
	}
	
	public void setPreferencesVisibility(boolean invisible) {
		if (menuCreated) {
			preferencesItem.setVisible(!invisible);
		}
	}
	
	public void waitForTagWithTitle(String title, WaitForTag status, boolean verbose) {
		getNdefMessage();
		
		if (status == WaitForTag.READ) {
			nfcHandler.prepareReading(title, verbose);
		} else if (status == WaitForTag.COPY) {
			nfcHandler.prepareCopy(title, verbose);
		} else if (status == WaitForTag.WRITE) {
			nfcHandler.prepareWriting(title, ndefMessage, verbose);
		} else if (status == WaitForTag.DELETE) {
			nfcHandler.prepareDelete(title, ndefMessage, verbose);
		}
		
		if (!verbose && getCurrentFragment() instanceof MainFragment) {
			((MainFragment)getCurrentFragment()).setWaitingTitle(title);
		}
	}
	
	public void waitForTag(WaitForTag status, boolean verbose) {
		waitForTagWithTitle(getResources().getString(R.string.approach_message), status, verbose);
	}
	
	public void doneWaitingForTag() {
		nfcHandler.setPending();
	}
	
	public void toggleMenuItems(final boolean homeAsUp, final boolean optionsVisible) {
		if (menuCreated) {
			materialMenu.animatePressedState(homeAsUp ? IconState.ARROW : IconState.BURGER);
			setPreferencesVisibility(optionsVisible);
		}
	}

	@Override
	public void onDismiss(DialogInterface dialog) {
		// if we do not have a "pending" status, the dialog was dismissed by the user, not by a read tag
		if (nfcHandler.getHandlerStatus() == HandlerStatus.WAITING_TO_DELETE) {
			if (getCurrentFragment() instanceof FragmentNFCWriteInterface) {
				((FragmentNFCWriteInterface) getCurrentFragment()).writeNFC();
			}
		} else if (nfcHandler.getHandlerStatus() == HandlerStatus.WAITING_FOR_COPY) {
			this.sendActionToDelegate(Actions.ACTIVITY_RESUME.toString());
		}
	}
	
	public NFCStatus getNfcStatus() {
		return nfcHandler.checkNfc();
	}
}
 
