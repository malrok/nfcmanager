package com.mrk.nfcmanager.ui.adapters;

import java.util.List;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mrk.mrkframework.viewmodel.interfaces.IViewModel;
import com.mrk.nfcmanager.R;
import com.mrk.nfcmanager.ext.dynamicgrid.BaseDynamicGridAdapter;
import com.mrk.nfcmanager.helpers.ActionViewRankHelper;
import com.mrk.nfcmanager.models.NFCRecord;
import com.mrk.nfcmanager.ui.views.ActionView;
import com.mrk.nfcmanager.viewmodels.RecordViewModel;

public class DragDropGridAdapter extends BaseDynamicGridAdapter {

	private Activity activity;
	
	public DragDropGridAdapter(Activity activity, List<IViewModel<NFCRecord>> items, int columnCount) {
		super(activity, items, columnCount);
		this.activity = activity;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.dragdrop_item, parent, false);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        
        holder.build(getItem(position));
        
        return convertView;
	}

	private class ViewHolder {
		private ActionView actionView;
		
		private ViewHolder(View view) {
			actionView = (ActionView) view.findViewById(R.id.dragdropaction);
        }

        void build(Object param) {
        	RecordViewModel vm = (RecordViewModel) param;
        	actionView.setContent(activity, vm, 
        			ActionViewRankHelper.getInstance(activity.getApplicationContext()).getNextColor(),
        			ActionViewRankHelper.getInstance(activity.getApplicationContext()).getNextGravity());
        }
	}
}
