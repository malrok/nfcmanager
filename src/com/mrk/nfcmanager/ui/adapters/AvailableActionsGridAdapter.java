package com.mrk.nfcmanager.ui.adapters;

import java.util.List;
import java.util.Map;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.mrk.mrkframework.viewmodel.interfaces.IViewModel;
import com.mrk.nfcmanager.R;
import com.mrk.nfcmanager.models.NFCRecord;
import com.mrk.nfcmanager.ui.views.ActionView;
import com.mrk.nfcmanager.viewmodels.AvailableActionsListViewModel;
import com.mrk.nfcmanager.viewmodels.RecordViewModel;
import com.tonicartos.widget.stickygridheaders.StickyGridHeadersBaseAdapter;

public class AvailableActionsGridAdapter extends BaseAdapter implements StickyGridHeadersBaseAdapter {

	private Context context;
	private List<IViewModel<NFCRecord>> items;
	private Map<String, Integer> categoriesMap;
	
	public AvailableActionsGridAdapter(Context context, AvailableActionsListViewModel items) {
		this.context = context;
		this.items = items.getListData();
		this.categoriesMap = items.getCategoriesMap();
	}
	
	@Override
	public int getCount() {
		return this.items.size();
	}
 
	@Override
	public Object getItem(int position) {
		return items.get(position);
	}
 
	@Override
	public long getItemId(int position) {
		return position;
	}
	
	@Override
	public int getCountForHeader(int position) {
		String header = (String) this.categoriesMap.keySet().toArray()[position];
		return this.categoriesMap.get(header);
	}

	@Override
	public int getNumHeaders() {
		return this.categoriesMap.keySet().toArray().length;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ItemViewHolder holder;
		
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.available_actions_item, parent, false);
            holder = new ItemViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ItemViewHolder) convertView.getTag();
        }
        
        holder.build(items.get(position));
        
        return convertView;
	}

	@Override
	public View getHeaderView(int position, View convertView, ViewGroup parent) {
		HeaderViewHolder holder;
		
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.available_actions_item_header, parent, false);
            holder = new HeaderViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (HeaderViewHolder) convertView.getTag();
        }
        
        holder.build((String) this.categoriesMap.keySet().toArray()[position]);
        
        return convertView;
	}

	private class ItemViewHolder {
		private ActionView actionView;
		
		private ItemViewHolder(View view) {
			actionView = (ActionView) view.findViewById(R.id.available_action);
        }

        void build(Object param) {
        	RecordViewModel vm = (RecordViewModel) param;
        	actionView.setContent(vm);
        }
	}
	
	private class HeaderViewHolder {
		private TextView title;
		
		private HeaderViewHolder(View view) {
			title = (TextView) view.findViewById(R.id.header_text);
        }

		void build(String param) {
        	title.setText(param);
        }
	}
}
