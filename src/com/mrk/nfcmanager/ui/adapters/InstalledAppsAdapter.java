package com.mrk.nfcmanager.ui.adapters;

import java.util.List;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mrk.nfcmanager.R;
import com.mrk.nfcmanager.models.InstalledApplication;

public class InstalledAppsAdapter extends ArrayAdapter<InstalledApplication> {

	private LayoutInflater inflater;

	public InstalledAppsAdapter(Context context, int resourceId, List<InstalledApplication> installedApplication) {
		super(context, resourceId, installedApplication.toArray(new InstalledApplication[installedApplication.size()]));
		this.inflater = LayoutInflater.from(context);
	}

	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent) {
		return getCustomView(position, convertView, parent);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		return getCustomView(position, convertView, parent);
	}

	private View getCustomView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.installed_app_row, parent, false);
			holder = new ViewHolder(convertView);
			
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		holder.build(getItem(position));
		
		return convertView;
	}

	private static class ViewHolder {
		private ImageView appIcon;
		private TextView appName;
		
		private ViewHolder(View view) {
			appIcon = (ImageView) view.findViewById(R.id.app_icon);
			appName = (TextView) view.findViewById(R.id.app_name);
        }

        void build(InstalledApplication app) {
        	if (app.getIcon() == null) {
        		Log.d("ViewHolder", "null");
        	}
        	appIcon.setImageDrawable(app.getIcon());
        	appName.setText(app.getTitle());
        }
	}
}
