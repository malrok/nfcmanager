package com.mrk.nfcmanager.ui.adapters;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.mrk.nfcmanager.R;
import com.mrk.nfcmanager.viewmodels.TagViewModel;

public class PayloadsListAdapter extends BaseAdapter {

	private LayoutInflater inflater;
	private List<NdefRecord> dataList;
	private String[] titleLine;
	
	private int titleBackgroundColor;
	
	public PayloadsListAdapter(Context context, TagViewModel viewModel) {
		this.inflater = LayoutInflater.from(context);
		this.dataList = new ArrayList<NdefRecord>(); 
		
		titleLine = new String[3];
		titleLine[0] = context.getResources().getString(R.string.payload_title_number);
		titleLine[1] = context.getResources().getString(R.string.payload_title_type);
		titleLine[2] = context.getResources().getString(R.string.payload_title_payload);
		
		titleBackgroundColor = context.getResources().getColor(R.color.light_blue);
		
		for (NdefMessage message : viewModel.getRawTagMessages()) {
			for (NdefRecord record : message.getRecords()) {
				this.dataList.add(record);
			}
		}
	}
	
	@Override
	public int getCount() {
		return dataList.size() + 1;
	}
	
	@Override
	public NdefRecord getItem(int position) {
	    return dataList.get(position);
	}
	
	@Override
	public long getItemId(int position) {
	    return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {

		ViewHolder holder;
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.tag_payloads_item, parent, false);
			
			holder = new ViewHolder();
			holder.messageNumber = (TextView) convertView.findViewById(R.id.message_number);
			holder.recordType = (TextView) convertView.findViewById(R.id.payload_type);
	    	holder.recordPayload = (TextView) convertView.findViewById(R.id.payload_content);
			
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		
		if (position == 0) {
			holder.messageNumber.setText(titleLine[0]);
			holder.recordType.setText(titleLine[1]);
			holder.recordPayload.setText(titleLine[2]);
			
			holder.recordType.setBackgroundColor(titleBackgroundColor);
			holder.recordPayload.setBackgroundColor(titleBackgroundColor);
		} else {
			holder.messageNumber.setText(Integer.toString(position - 1));
			holder.recordType.setText(new String(dataList.get(position - 1).getType()));
			
			byte[] payload = dataList.get(position - 1).getPayload();
			
			if (payload != null) {
				holder.recordPayload.setText(new String(payload));
			}
		}
		
		return convertView;
	}

	private class ViewHolder {
    	TextView messageNumber;
    	TextView recordType;
    	TextView recordPayload;
    }
	
}
