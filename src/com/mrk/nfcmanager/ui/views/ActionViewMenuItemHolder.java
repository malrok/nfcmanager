package com.mrk.nfcmanager.ui.views;

import java.util.List;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.Spinner;
import android.widget.TextView;

import com.mrk.nfcmanager.R;
import com.mrk.nfcmanager.helpers.InstalledApplicationHelper;
import com.mrk.nfcmanager.models.InstalledApplication;
import com.mrk.nfcmanager.models.nfcactions.abstracts.EnumWrapper;
import com.mrk.nfcmanager.models.nfcactions.abstracts.InstalledAppsWrapper;
import com.mrk.nfcmanager.models.nfcactions.abstracts.ResourceEnum;
import com.mrk.nfcmanager.ui.adapters.InstalledAppsAdapter;

public class ActionViewMenuItemHolder {

	private Context context;
	
	private ItemHolderModification itf;
	private LinearLayout layout;
	
	private InstalledAppsAdapter adapter;
	private Spinner spinner;
	
	public ActionViewMenuItemHolder(Context context, String labelText, Object content, ItemHolderModification itf) {
		this.context = context;
		this.itf = itf;
		
		layout = new LinearLayout(context);
		
		LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		
		layout.setLayoutParams(layoutParams);
		layout.setOrientation(LinearLayout.VERTICAL);
		
		if (labelText != null) {
			TextView label= new TextView(context);
			label.setText(labelText);
			layout.addView(label);
		}
		
		if (content instanceof EnumWrapper) {
			createRadio(context, (EnumWrapper<?>)content);
		} else if (content instanceof InstalledAppsWrapper) {
			createSpinner(context, (InstalledAppsWrapper)content);
		} else if (content instanceof String) {
			createText(context, (String)content);
		}
	}

	public View getView() {
		return layout;
	}
	
	private void createRadio(Context context, EnumWrapper<?> content) {
		RadioGroup radio = new RadioGroup(context);
		
		for (int rank = 0; rank < content.getItems().length ; rank ++) {
			Enum<?> item = content.getItems()[rank];
			RadioButton radioButton = new RadioButton(context);
			radioButton.setText(context.getResources().getString(((ResourceEnum)item).getResource()));
			radioButton.setId(rank);
			
			if (rank == content.getSelectedItem()) {
				radioButton.setChecked(true);
			}
			
			radio.addView(radioButton);
		}
		
		radio.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				itf.modified(RadioGroup.class, checkedId);
			}
		});
		
		layout.addView(radio);
	}
	
	private void createSpinner(Context context, InstalledAppsWrapper content) {
		spinner = new Spinner(context);
		
		spinner.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> adapter, View view, int position, long id) {
				itf.modified(Spinner.class, ((InstalledApplication)adapter.getItemAtPosition(position)).getPack());
			}

			@Override
			public void onNothingSelected(AdapterView<?> adapter) { }
			
		});
		
		adapter = new InstalledAppsAdapter(context, R.layout.installed_app_row, InstalledApplicationHelper.getInstance(context).getApplicationsList(this, content.getFilter()));
		spinner.setAdapter(adapter);
		spinner.setSelection(InstalledApplicationHelper.getInstance(context).getPackageIndex(content.getSelectedApp()));
		
		layout.addView(spinner);
	}
	
	private void createText(Context context, String content) {
		final EditText text = new EditText(context);
		text.setText(content);
		
		text.addTextChangedListener(new TextWatcher() {
			
			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {
				itf.modified(EditText.class, text.getText().toString());
			}
			
			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {
				
			}
			
			@Override
			public void afterTextChanged(Editable s) {
				
			}
		});
		
		layout.addView(text);
	}
	
	public interface ItemHolderModification {
		public void modified(Class<?> objectType, Object newValue);
	}
	
	public void updateSpinnerAdapter(List<InstalledApplication> installedApps) {
		if (this.adapter != null) {
			this.adapter = new InstalledAppsAdapter(context, R.layout.installed_app_row, installedApps);
			this.spinner.setAdapter(adapter);
		}
	}
}
