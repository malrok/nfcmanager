package com.mrk.nfcmanager.ui.views;

import android.content.Context;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.Spinner;

import com.mrk.nfcmanager.models.nfcactions.abstracts.AbstractActionData;
import com.mrk.nfcmanager.models.nfcactions.abstracts.EnumWrapper;
import com.mrk.nfcmanager.models.nfcactions.abstracts.InstalledAppsWrapper;
import com.mrk.nfcmanager.ui.views.ActionViewMenuItemHolder.ItemHolderModification;
import com.mrk.nfcmanager.viewmodels.ActionClassViewModel;

public class ActionViewMenu extends LinearLayout {

	private Context context;
	
	private ActionClassViewModel vm;
	
	public ActionViewMenu(Context context) {
		super(context);
		
		LinearLayout.LayoutParams layout = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		
		this.setLayoutParams(layout);
		this.setOrientation(LinearLayout.VERTICAL);
		
		this.context = context;
	}

	public void create(ActionClassViewModel vm) {
		this.vm = vm;
		
		if (vm.canModifyTnf()) {
			ActionViewMenuItemHolder holder = new ActionViewMenuItemHolder(context, "Tnf", String.valueOf(vm.getTnf()), new ItemHolderModification() {
				@Override
				public void modified(Class<?> objectType, Object newValue) {
					ActionViewMenu.this.vm.setTnf(Short.valueOf((String)newValue));
				}
			});
			this.addView(holder.getView());
		}
		
		if (vm.canModifyType()) {
			ActionViewMenuItemHolder holder = new ActionViewMenuItemHolder(context, "Type", String.valueOf(vm.getType()), new ItemHolderModification() {
				@Override
				public void modified(Class<?> objectType, Object newValue) {
					ActionViewMenu.this.vm.setType(((String)newValue).getBytes());
				}
			});
			this.addView(holder.getView());
		}
		
		for (int rank = 0; rank < vm.getData().getPayloadConfig().size() ; rank++) {
			final AbstractActionData payloadData = vm.getData().getPayloadConfig().get(rank);
			ActionViewMenuItemHolder holder = new ActionViewMenuItemHolder(context, payloadData.getLabel(), payloadData.getData(), new ItemHolderModification() {
				@Override
				public void modified(Class<?> objectType, Object newValue) {
					if (objectType.equals(RadioGroup.class)) {
						((EnumWrapper<?>)payloadData.getData()).setSelectedItem((int) newValue);
					} else if (objectType.equals(Spinner.class)) {
						((InstalledAppsWrapper)payloadData.getData()).setSelectedApp((String) newValue);
					} else { // EditText
						payloadData.setData(newValue);
					}
				}
			});
			this.addView(holder.getView());
		}
	}
	
	public ActionClassViewModel getViewModel() {
		return vm;
	}
}
