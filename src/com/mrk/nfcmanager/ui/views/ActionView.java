package com.mrk.nfcmanager.ui.views;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnDismissListener;
import android.nfc.NdefRecord;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mrk.mrkframework.ui.AbstractMRKFragmentActivity;
import com.mrk.nfcmanager.R;
import com.mrk.nfcmanager.dataloaders.InternalActionsDataLoader;
import com.mrk.nfcmanager.models.nfcactions.interfaces.ActionClassInterface;
import com.mrk.nfcmanager.ui.fragments.TagWritingFragment;
import com.mrk.nfcmanager.viewmodels.RecordViewModel;

public class ActionView extends LinearLayout {

	/** context */
	private Context context;
	
	/** views */
	private TextView actionNumber;
	private ImageView actionImage;
	private ImageView actionCaption;
	private TextView actionTitle;
	private Dialog actionPopup;
	
	/** vm */
	private RecordViewModel vm;
	
	public ActionView(Context context) {
		this(context, null);
	}

	public ActionView(Context context, AttributeSet attrs) {
		this(context, attrs, 0);
	}

	public ActionView(Context context, AttributeSet attrs, int defStyleAttr) {
		super(context, attrs, defStyleAttr);
		
		this.context = context;
		
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		inflater.inflate(R.layout.action_view, this, true);
		actionNumber = (TextView) findViewById(R.id.action_number);
		actionImage = (ImageView) findViewById(R.id.action_image);
		actionCaption = (ImageView) findViewById(R.id.action_caption);
		actionTitle = (TextView) findViewById(R.id.action_title);
	}
	
	@Override
	public void onDetachedFromWindow() {
		if (actionPopup != null) {
			actionPopup.dismiss();
		}
		super.onDetachedFromWindow();
	}
	
	public void setContent(RecordViewModel vm) {
		this.vm = vm;
		
		actionNumber.setText("");
		actionImage.setImageDrawable(vm.getData().getIconDrawable(context));
		actionTitle.setText(vm.getData().getActionTitle());
		
		if (vm.getData().getTnf() == NdefRecord.TNF_WELL_KNOWN) {
			actionCaption.setBackground(getResources().getDrawable(R.drawable.universal_circle));
		} else if (vm.getData().isInternal()){
			actionCaption.setBackground(getResources().getDrawable(R.drawable.internal_circle));
		}
	}
	
	public void setContent(final Activity activity, final RecordViewModel vm, int color, int gravity) {
		this.vm = vm;
		
		InternalActionsDataLoader actionsDataloader = new InternalActionsDataLoader();
		
		actionNumber.setText(Integer.toString(vm.getData().getRank()));
		actionImage.setImageDrawable(vm.getData().getIconDrawable(context));
		
		FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        params.gravity = gravity;
        params.setMargins(10, 10, 10, 10);
        
        actionNumber.setBackgroundColor(color);
		actionNumber.setLayoutParams(params);
		actionNumber.setPadding(10, 10, 10, 10);
		
		final ActionViewMenu actionMenuView = new ActionViewMenu(this.context);
		ActionClassInterface actionClass = actionsDataloader.getActionFromType(this.context, vm.getData().getActionType());
		actionMenuView.create(actionsDataloader.loadFromActionClass(this.context, actionClass, vm));
		actionPopup = new Dialog(activity);
		actionPopup.requestWindowFeature(Window.FEATURE_NO_TITLE);
		actionPopup.setContentView(actionMenuView, params);
		actionPopup.setOnDismissListener(new OnDismissListener() {
			@Override
			public void onDismiss(DialogInterface dialog) {
				actionMenuView.getViewModel().computePayload();
				vm.updateFromActionClassViewModel(actionMenuView.getViewModel());
				if (activity instanceof AbstractMRKFragmentActivity && ((AbstractMRKFragmentActivity)activity).getCurrentFragment() instanceof TagWritingFragment) {
					((TagWritingFragment)((AbstractMRKFragmentActivity)activity).getCurrentFragment()).updateUsedSize();
				}
			}
		});
		
	}
	
	public RecordViewModel getViewModel() {
		return this.vm;
	}

	public void onClick() {
		if (actionPopup!= null && !actionPopup.isShowing()) {
			actionPopup.show();
		}
	}
	
}
