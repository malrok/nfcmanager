package com.mrk.nfcmanager.models.nfcactions;

import android.content.Context;
import android.nfc.NdefRecord;

import com.mrk.nfcmanager.R;
import com.mrk.nfcmanager.helpers.ActionsCategories;
import com.mrk.nfcmanager.models.nfcactions.abstracts.AbstractActionClass;
import com.mrk.nfcmanager.models.nfcactions.abstracts.AbstractActionData;
import com.mrk.nfcmanager.models.nfcactions.interfaces.ActionClassInterface;

public class WKTextActionClass extends AbstractActionClass implements ActionClassInterface {

	public final static byte[] WK_TEXT = new byte[] { 0x1 };
	
	public WKTextActionClass(Context context) {
		super(context);
		defaultTnf = NdefRecord.TNF_WELL_KNOWN;
		defaultType = WK_TEXT;
		payloadConfig.add(new AbstractActionData(context.getResources().getString(R.string.type_text_message), new String()));
		category = ActionsCategories.WELLKNOWN.getLabel(context);
		icon = "ic_text";
		modificationAuthorizations = new boolean[] { false, false };
		title = context.getResources().getString(R.string.wktext_action_title);
		isInternal = false;
	}
	
	@Override
	public void execute(Context context, String param[]) {
		// nothing to do, wellknown type ; processed by system
	}
	
}
