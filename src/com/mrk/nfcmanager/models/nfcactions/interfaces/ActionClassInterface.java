package com.mrk.nfcmanager.models.nfcactions.interfaces;

import java.util.List;

import android.content.Context;

import com.mrk.nfcmanager.models.nfcactions.abstracts.AbstractActionData;

public interface ActionClassInterface {

	public void execute(Context context, String param[]);
	
	public Object getTnfConfig();
	
	public short getDefaultTnf();
	
	public void setDefaultTnf(short defaultTnf);
	
	public Object getTypeConfig();
	
	public Object getDefaultType();
	
	public void setDefaultType(Object defaultType);
	
	public List<AbstractActionData> getPayloadConfig();
	
	public String getCategory();
	
	public String getIcon();
	
	public String getTitle();
	
	public boolean[] getModificationAuthorizations();

	public boolean isInternal();
	
}
