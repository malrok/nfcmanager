package com.mrk.nfcmanager.models.nfcactions;

import android.content.Context;
import android.nfc.NdefRecord;

import com.mrk.nfcmanager.R;
import com.mrk.nfcmanager.helpers.ActionsCategories;
import com.mrk.nfcmanager.models.nfcactions.abstracts.AbstractActionClass;
import com.mrk.nfcmanager.models.nfcactions.abstracts.AbstractActionData;
import com.mrk.nfcmanager.models.nfcactions.interfaces.ActionClassInterface;

public class WKUriActionClass extends AbstractActionClass implements ActionClassInterface {

	public final static byte[] WK_URI = new byte[] { 0x12 };
	
	public WKUriActionClass(Context context) {
		super(context);
		defaultTnf = NdefRecord.TNF_WELL_KNOWN;
		defaultType = WK_URI;
		payloadConfig.add(new AbstractActionData(context.getResources().getString(R.string.type_url_message), new String()));
		category = ActionsCategories.WELLKNOWN.getLabel(context);
		icon = "ic_url";
		modificationAuthorizations = new boolean[] { false, false };
		title = context.getResources().getString(R.string.wkurl_action_title);
		isInternal = false;
	}
	
	@Override
	public void execute(Context context, String param[]) {
		// nothing to do, wellknown type ; processed by system
	}
	
}
