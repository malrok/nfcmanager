package com.mrk.nfcmanager.models.nfcactions;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.nfc.NdefRecord;

import com.mrk.nfcmanager.R;
import com.mrk.nfcmanager.helpers.ActionsCategories;
import com.mrk.nfcmanager.models.nfcactions.abstracts.AbstractActionClass;
import com.mrk.nfcmanager.models.nfcactions.abstracts.AbstractActionData;
import com.mrk.nfcmanager.models.nfcactions.abstracts.EnumWrapper;
import com.mrk.nfcmanager.models.nfcactions.abstracts.ResourceEnum;
import com.mrk.nfcmanager.models.nfcactions.interfaces.ActionClassInterface;

public class DataActionClass extends AbstractActionClass implements ActionClassInterface {

	public final static byte[] DATA_TYPE = new byte[] { 0x7 };

	private enum Status implements ResourceEnum {
		ON(R.string.on), OFF(R.string.off), TOGGLE(R.string.toggle);

		private int resource;

		private Status(int resource) {
			this.resource = resource;
		}

		public int getResource() {
			return this.resource;
		}
	}

	public DataActionClass(Context context) {
		super(context);
		defaultTnf = NdefRecord.TNF_MIME_MEDIA;
		;
		defaultType = DATA_TYPE;
		payloadConfig.add(new AbstractActionData(context.getResources().getString(R.string.select_option_message),
				new EnumWrapper<Status>(Status.class)));
		category = ActionsCategories.CONNECTIVITY.getLabel(context);
		icon = "ic_network_cell";
		modificationAuthorizations = new boolean[] { false, false };
		title = context.getResources().getString(R.string.data_action_title);
	}

	@Override
	public void execute(Context context, String param[]) {
		ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

		boolean setOn = param[0].trim().equalsIgnoreCase(Status.ON.toString())
				|| (param[0].trim().equalsIgnoreCase(Status.TOGGLE.toString()) && !(activeNetwork != null && activeNetwork
						.getType() == ConnectivityManager.TYPE_MOBILE));

		try {
			setMobileDataEnabled(context, setOn);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException
				| ClassNotFoundException | NoSuchFieldException e) {
			e.printStackTrace();
		}
	}

	// private void setMobileDataEnabled(Context context, boolean enabled)
	// throws IllegalAccessException, IllegalArgumentException,
	// InvocationTargetException, NoSuchMethodException, ClassNotFoundException,
	// NoSuchFieldException {
	// final ConnectivityManager conman = (ConnectivityManager)
	// context.getSystemService(Context.CONNECTIVITY_SERVICE);
	// final Class<?> conmanClass = Class.forName(conman.getClass().getName());
	// final Field iConnectivityManagerField =
	// conmanClass.getDeclaredField("mService");
	//
	// iConnectivityManagerField.setAccessible(true);
	//
	// final Object iConnectivityManager =
	// iConnectivityManagerField.get(conman);
	// final Class<?> iConnectivityManagerClass =
	// Class.forName(iConnectivityManager.getClass().getName());
	// final Method setMobileDataEnabledMethod =
	// iConnectivityManagerClass.getDeclaredMethod("setMobileDataEnabled",
	// boolean.class);
	//
	// setMobileDataEnabledMethod.setAccessible(true);
	// setMobileDataEnabledMethod.invoke(iConnectivityManager, enabled);
	// }

	private void setMobileDataEnabled(Context context, boolean enabled) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, ClassNotFoundException, NoSuchFieldException {
		ConnectivityManager dataManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		Method dataMtd = ConnectivityManager.class.getDeclaredMethod("setMobileDataEnabled", boolean.class);
		dataMtd.setAccessible(true);
		dataMtd.invoke(dataManager, enabled);
	}
}
