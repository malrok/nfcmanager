package com.mrk.nfcmanager.models.nfcactions;

import android.content.Context;
import android.media.AudioManager;
import android.nfc.NdefRecord;

import com.mrk.nfcmanager.R;
import com.mrk.nfcmanager.helpers.ActionsCategories;
import com.mrk.nfcmanager.models.nfcactions.abstracts.AbstractActionClass;
import com.mrk.nfcmanager.models.nfcactions.abstracts.AbstractActionData;
import com.mrk.nfcmanager.models.nfcactions.abstracts.EnumWrapper;
import com.mrk.nfcmanager.models.nfcactions.abstracts.ResourceEnum;
import com.mrk.nfcmanager.models.nfcactions.interfaces.ActionClassInterface;

public class VolumeActionClass extends AbstractActionClass implements ActionClassInterface {

	public final static byte[] VOLUME_TYPE = new byte[] { 0x5 };
	
	private enum Status implements ResourceEnum {
		MAX(R.string.max),
		VIBRATE(R.string.vibrate),
		MUTE(R.string.mute);
		
		private int resource;
		
		private Status(int resource) {
			this.resource = resource;
		}
		
		public int getResource() {
			return this.resource;
		}
	}
	
	public VolumeActionClass(Context context) {
		super(context);
		defaultTnf = NdefRecord.TNF_MIME_MEDIA;
		defaultType = VOLUME_TYPE;
		payloadConfig.add(new AbstractActionData(context.getResources().getString(R.string.select_option_message), new EnumWrapper<Status>(Status.class)));
		category = ActionsCategories.MEDIA.getLabel(context);
		icon = "ic_volume";
		modificationAuthorizations = new boolean[] { false, false };
		title = context.getResources().getString(R.string.volume_action_title);
	}
	
	@Override
	public void execute(Context context, String param[]) {
		AudioManager audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
		
		if (param[0].trim().equals(Status.MAX.toString())) {
			audioManager.setStreamVolume(AudioManager.STREAM_RING, audioManager.getStreamMaxVolume(AudioManager.STREAM_RING), 0);
			audioManager.setStreamVolume(AudioManager.STREAM_NOTIFICATION, audioManager.getStreamMaxVolume(AudioManager.STREAM_NOTIFICATION), 0);
			audioManager.setStreamVolume(AudioManager.STREAM_SYSTEM, audioManager.getStreamMaxVolume(AudioManager.STREAM_SYSTEM), 0);
		}
		if (param[0].trim().equals(Status.MUTE.toString())) {
			audioManager.setRingerMode(AudioManager.RINGER_MODE_SILENT);
		}
		if (param[0].trim().equals(Status.VIBRATE.toString())) {
			audioManager.setRingerMode(AudioManager.RINGER_MODE_VIBRATE);
		}
	}
}
