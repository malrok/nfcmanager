package com.mrk.nfcmanager.models.nfcactions;

import java.lang.reflect.Method;

import android.content.Context;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.nfc.NdefRecord;

import com.mrk.nfcmanager.R;
import com.mrk.nfcmanager.helpers.ActionsCategories;
import com.mrk.nfcmanager.models.nfcactions.abstracts.AbstractActionClass;
import com.mrk.nfcmanager.models.nfcactions.abstracts.AbstractActionData;
import com.mrk.nfcmanager.models.nfcactions.abstracts.EnumWrapper;
import com.mrk.nfcmanager.models.nfcactions.abstracts.ResourceEnum;
import com.mrk.nfcmanager.models.nfcactions.interfaces.ActionClassInterface;

public class WifiHotSpotActionClass extends AbstractActionClass implements ActionClassInterface {

	public final static byte[] WIFI_HOTSPOT_TYPE = new byte[] { 0x9 };

	private enum Status implements ResourceEnum {
		ON(R.string.on), OFF(R.string.off), TOGGLE(R.string.toggle);

		private int resource;

		private Status(int resource) {
			this.resource = resource;
		}

		public int getResource() {
			return this.resource;
		}
	}
	
	private static final int WIFI_AP_STATE_FAILED = 4;
	private static final int WIFI_AP_STATE_ON = 13;

	public WifiHotSpotActionClass(Context context) {
		super(context);
		defaultTnf = NdefRecord.TNF_MIME_MEDIA;
		defaultType = WIFI_HOTSPOT_TYPE;
		payloadConfig.add(new AbstractActionData(context.getResources().getString(R.string.select_option_message), new EnumWrapper<Status>(Status.class)));
		category = ActionsCategories.CONNECTIVITY.getLabel(context);
		icon = "ic_wifi_tethering";
		modificationAuthorizations = new boolean[] { false, false };
		title = context.getResources().getString(R.string.hotspot_action_title);
	}

	@Override
	public void execute(Context context, String param[]) {
		WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
		
		int state = getWifiApState(wifiManager);
		
		boolean enable = param[0].trim().equalsIgnoreCase(Status.ON.toString()) || (param[0].trim().equalsIgnoreCase(Status.TOGGLE.toString()) && state != WIFI_AP_STATE_ON);
		
		setWifiApState(context, wifiManager, null, enable);
	}

	public int getWifiApState(WifiManager wifiManager) {
		try {
			Method wifiApState = wifiManager.getClass().getMethod("getWifiApState");
			return (Integer) wifiApState.invoke(wifiManager);
		} catch (Exception e) {
			e.printStackTrace();
			return WIFI_AP_STATE_FAILED;
		}
	}

	public boolean setWifiApState(Context context, WifiManager wifiManager, WifiConfiguration config, boolean enabled) {
		try {
			Method wifiControlMethod = wifiManager.getClass().getMethod("setWifiApEnabled", WifiConfiguration.class,
					boolean.class);
			if (enabled) {
				wifiManager.setWifiEnabled(!enabled);
			}
			return (Boolean) wifiControlMethod.invoke(wifiManager, config, enabled);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
}
