package com.mrk.nfcmanager.models.nfcactions;

import android.content.Context;
import android.nfc.NdefRecord;

import com.mrk.nfcmanager.R;
import com.mrk.nfcmanager.helpers.ActionsCategories;
import com.mrk.nfcmanager.helpers.InstalledApplicationHelper;
import com.mrk.nfcmanager.models.nfcactions.abstracts.AbstractActionClass;
import com.mrk.nfcmanager.models.nfcactions.abstracts.AbstractActionData;
import com.mrk.nfcmanager.models.nfcactions.abstracts.InstalledAppsWrapper;
import com.mrk.nfcmanager.models.nfcactions.interfaces.ActionClassInterface;

public class LaunchAppActionClass extends AbstractActionClass implements ActionClassInterface {

	public final static byte[] LAUNCH_APP_TYPE = new byte[] { 0x10 };

	public LaunchAppActionClass(Context context) {
		super(context);
		defaultTnf = NdefRecord.TNF_MIME_MEDIA;
		defaultType = LAUNCH_APP_TYPE;
		payloadConfig.add(new AbstractActionData(context.getResources().getString(R.string.select_app_message), new InstalledAppsWrapper()));
		category = ActionsCategories.EXECUTIONS.getLabel(context);
		icon = "ic_apps";
		modificationAuthorizations = new boolean[] { false, false };
		title = context.getResources().getString(R.string.apps_action_title);
	}

	@Override
	public void execute(Context context, String[] strings) {
		InstalledApplicationHelper.launchAppFromPackage(context, strings[0]);
	}
	
}
