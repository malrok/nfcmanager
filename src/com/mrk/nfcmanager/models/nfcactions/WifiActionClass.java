package com.mrk.nfcmanager.models.nfcactions;

import android.content.Context;
import android.net.wifi.WifiManager;
import android.nfc.NdefRecord;

import com.mrk.nfcmanager.R;
import com.mrk.nfcmanager.helpers.ActionsCategories;
import com.mrk.nfcmanager.models.nfcactions.abstracts.AbstractActionClass;
import com.mrk.nfcmanager.models.nfcactions.abstracts.AbstractActionData;
import com.mrk.nfcmanager.models.nfcactions.abstracts.EnumWrapper;
import com.mrk.nfcmanager.models.nfcactions.abstracts.ResourceEnum;
import com.mrk.nfcmanager.models.nfcactions.interfaces.ActionClassInterface;

public class WifiActionClass extends AbstractActionClass implements ActionClassInterface {

	public final static byte[] WIFI_TYPE = new byte[] { 0x3 };
	
	private enum Status implements ResourceEnum {
		ON(R.string.on),
		OFF(R.string.off), 
		TOGGLE(R.string.toggle);
		
		private int resource;
		
		private Status(int resource) {
			this.resource = resource;
		}
		
		public int getResource() {
			return this.resource;
		}
	}
	
	public WifiActionClass(Context context) {
		super(context);
		defaultTnf = NdefRecord.TNF_MIME_MEDIA;
		defaultType = WIFI_TYPE;
		payloadConfig.add(new AbstractActionData(context.getResources().getString(R.string.select_option_message), new EnumWrapper<Status>(Status.class)));
		category = ActionsCategories.CONNECTIVITY.getLabel(context);
		icon = "ic_network_wifi";
		modificationAuthorizations = new boolean[] { false, false };
		title = context.getResources().getString(R.string.wifi_action_title);
	}
	
	@Override
	public void execute(Context context, String param[]) {
		WifiManager wifi = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
		
		boolean setOn = param[0].trim().equalsIgnoreCase(Status.ON.toString()) || (param[0].trim().equalsIgnoreCase(Status.TOGGLE.toString()) && !wifi.isWifiEnabled());
		
		wifi.setWifiEnabled(setOn);
	}

}
