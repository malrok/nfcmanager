package com.mrk.nfcmanager.models.nfcactions;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.nfc.NdefRecord;
import android.view.KeyEvent;

import com.mrk.nfcmanager.R;
import com.mrk.nfcmanager.helpers.ActionsCategories;
import com.mrk.nfcmanager.models.nfcactions.abstracts.AbstractActionClass;
import com.mrk.nfcmanager.models.nfcactions.abstracts.AbstractActionData;
import com.mrk.nfcmanager.models.nfcactions.abstracts.InstalledAppsWrapper;
import com.mrk.nfcmanager.models.nfcactions.interfaces.ActionClassInterface;

public class PlayMusicActionClass extends AbstractActionClass implements ActionClassInterface {

	public final static byte[] MUSIC_TYPE = new byte[] { 0x11 };

	public PlayMusicActionClass(Context context) {
		super(context);
		defaultTnf = NdefRecord.TNF_MIME_MEDIA;
		defaultType = MUSIC_TYPE;
		payloadConfig.add(new AbstractActionData(context.getResources().getString(R.string.select_app_message), new InstalledAppsWrapper(getFilter(context))));
		category = ActionsCategories.EXECUTIONS.getLabel(context);
		icon = "ic_music";
		modificationAuthorizations = new boolean[] { false, false };
		title = context.getResources().getString(R.string.music_action_title);
	}

	@Override
	public void execute(Context context, String[] strings) {
		play(context, strings[0]);
	}

	private void play(final Context context, final String appPackage) {
		Intent downIntent = new Intent(Intent.ACTION_MEDIA_BUTTON);
		downIntent.putExtra(Intent.EXTRA_KEY_EVENT, new KeyEvent(KeyEvent.ACTION_DOWN,
				KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE));
		downIntent.setPackage(appPackage);
		context.sendOrderedBroadcast(downIntent, null);

		Intent upIntent = new Intent(Intent.ACTION_MEDIA_BUTTON);
		upIntent.putExtra(Intent.EXTRA_KEY_EVENT, new KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE));
		upIntent.setPackage(appPackage);
		context.sendOrderedBroadcast(upIntent, null);
	}

	private List<String> getFilter(Context context) {
		List<String> filter = new ArrayList<String>();
		try {
			InputStream inputStream = context.getResources().openRawResource(R.raw.music_apps_packages);

			BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
			
			String line;
			
			while (( line = reader.readLine()) != null) {
				filter.add(line);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return filter;
	}
}
