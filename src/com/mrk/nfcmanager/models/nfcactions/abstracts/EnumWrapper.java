package com.mrk.nfcmanager.models.nfcactions.abstracts;

public class EnumWrapper<E extends Enum<E>> {
	
	private E[] items;
	private int selectedItem = 0;
	
	public EnumWrapper(Class<E> item){
	    this.items = item.getEnumConstants();
	}
	
	public E[] getItems(){
	    return items;
	}
	
	public E getSelection() {
		return this.items[this.selectedItem];
	}
	
	public void setSelection(Enum<E> enumeration) {
		this.selectedItem = enumeration.ordinal();
	}
	
	public int getSelectedItem() {
	    return selectedItem;
	}
	
	public void setSelectedItem(int selectedItem) {
	    this.selectedItem = selectedItem;
	}
}
