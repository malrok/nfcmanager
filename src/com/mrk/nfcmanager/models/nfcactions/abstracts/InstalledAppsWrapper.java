package com.mrk.nfcmanager.models.nfcactions.abstracts;

import java.util.List;


public class InstalledAppsWrapper {

	private String selectedApp = "";
	private List<String> filter;

	public InstalledAppsWrapper() {
		// nothing to do
	}
	
	public InstalledAppsWrapper(List<String> filter) {
		this.setFilter(filter);
	}
	
	public String getSelectedApp() {
		return selectedApp;
	}

	public void setSelectedApp(String selectedApp) {
		this.selectedApp = selectedApp;
	}

	public List<String> getFilter() {
		return filter;
	}

	public void setFilter(List<String> filter) {
		this.filter = filter;
	}
	
}
