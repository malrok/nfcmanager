package com.mrk.nfcmanager.models.nfcactions.abstracts;

public interface ResourceEnum {

	public int getResource();
	
}
