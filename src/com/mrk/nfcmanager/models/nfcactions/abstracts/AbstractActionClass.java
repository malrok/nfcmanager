package com.mrk.nfcmanager.models.nfcactions.abstracts;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;

import com.mrk.mrkframework.model.ModelInterface;

public abstract class AbstractActionClass implements ModelInterface {
	
	public static final int TNF_MODIFY = 0;
	public static final int TYPE_MODIFY = 0;
	
	protected Object tnfConfig;
	protected short defaultTnf;
	protected Object typeConfig;
	protected Object defaultType;
	protected List<AbstractActionData> payloadConfig;
	protected String category;
	protected String icon;
	protected String title;
	protected boolean[] modificationAuthorizations;
	protected boolean isInternal = true;
	
	public abstract void execute(Context context, String[] strings);
	
	public AbstractActionClass(Context context) {
		payloadConfig = new ArrayList<AbstractActionData>();
		modificationAuthorizations  = new boolean[2]; 
	}

	public Object getTnfConfig() {
		return tnfConfig;
	}

	public void setTnfConfig(Object tnfConfig) {
		this.tnfConfig = tnfConfig;
	}

	public Object getTypeConfig() {
		return typeConfig;
	}

	public void setTypeConfig(Object typeConfig) {
		this.typeConfig = typeConfig;
	}
	
	public List<AbstractActionData> getPayloadConfig() {
		return payloadConfig;
	}

	public void setPayloadConfig(List<AbstractActionData> payloadConfig) {
		this.payloadConfig = payloadConfig;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public boolean[] getModificationAuthorizations() {
		return modificationAuthorizations;
	}

	public void setTnfModificationAuthorization(boolean modificationAuthorization) {
		this.modificationAuthorizations[TNF_MODIFY] = modificationAuthorization;
	}
	
	public void setTypeModificationAuthorization(boolean modificationAuthorization) {
		this.modificationAuthorizations[TYPE_MODIFY] = modificationAuthorization;
	}
	
	public short getDefaultTnf() {
		return this.defaultTnf;
	}
	
	public void setDefaultTnf(short defaultTnf) {
		this.defaultTnf = defaultTnf;
	}
	
	public Object getDefaultType() {
		return this.defaultType;
	}
	
	public void setDefaultType(Object defaultType) {
		this.defaultType = defaultType;
	}
	
	public boolean isInternal() {
		return this.isInternal;
	}
	
	public void setIsInternal(boolean isInternal) {
		this.isInternal = isInternal;
	}
	
	@Override
	public AbstractActionClass clone() {
		return null;
	}
}
