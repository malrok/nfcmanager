package com.mrk.nfcmanager.models.nfcactions;

import android.bluetooth.BluetoothAdapter;
import android.content.Context;
import android.nfc.NdefRecord;

import com.mrk.nfcmanager.R;
import com.mrk.nfcmanager.helpers.ActionsCategories;
import com.mrk.nfcmanager.models.nfcactions.abstracts.AbstractActionClass;
import com.mrk.nfcmanager.models.nfcactions.abstracts.AbstractActionData;
import com.mrk.nfcmanager.models.nfcactions.abstracts.EnumWrapper;
import com.mrk.nfcmanager.models.nfcactions.abstracts.ResourceEnum;
import com.mrk.nfcmanager.models.nfcactions.interfaces.ActionClassInterface;

public class BluetoothActionClass extends AbstractActionClass implements ActionClassInterface {

	public final static byte[] BLUETOOTH_TYPE = new byte[] { 0x4 };
	
	private enum Status implements ResourceEnum {
		ON(R.string.on),
		OFF(R.string.off), 
		TOGGLE(R.string.toggle);
		
		private int resource;
		
		private Status(int resource) {
			this.resource = resource;
		}
		
		public int getResource() {
			return this.resource;
		}
	}
	
	public BluetoothActionClass(Context context) {
		super(context);
		defaultTnf = NdefRecord.TNF_MIME_MEDIA;
		defaultType = BLUETOOTH_TYPE;
		payloadConfig.add(new AbstractActionData(context.getResources().getString(R.string.select_option_message), new EnumWrapper<Status>(Status.class)));
		category = ActionsCategories.CONNECTIVITY.getLabel(context);
		icon = "ic_bluetooth";
		modificationAuthorizations = new boolean[] { false, false };
		title = context.getResources().getString(R.string.bluetooth_action_title);
	}
	
	@Override
	public void execute(Context context, String param[]) {
		BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		
		boolean setOn = param[0].trim().equalsIgnoreCase(Status.ON.toString()) || (param[0].trim().equalsIgnoreCase(Status.TOGGLE.toString()) && !mBluetoothAdapter.isEnabled());
		
		if (setOn) {
			mBluetoothAdapter.enable();
		} else {
			mBluetoothAdapter.disable();
		}
	}
}
