package com.mrk.nfcmanager.models.nfcactions;

import android.content.Context;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.nfc.NdefRecord;
import android.widget.Toast;

import com.mrk.nfcmanager.R;
import com.mrk.nfcmanager.helpers.ActionsCategories;
import com.mrk.nfcmanager.models.nfcactions.abstracts.AbstractActionClass;
import com.mrk.nfcmanager.models.nfcactions.abstracts.AbstractActionData;
import com.mrk.nfcmanager.models.nfcactions.interfaces.ActionClassInterface;

public class WifiAddActionClass extends AbstractActionClass implements ActionClassInterface {

public final static byte[] WIFI_ADD_TYPE = new byte[] { 0x8 };
	
	public WifiAddActionClass(Context context) {
		super(context);
		defaultTnf = NdefRecord.TNF_MIME_MEDIA;
		defaultType = WIFI_ADD_TYPE;
		payloadConfig.add(new AbstractActionData(context.getResources().getString(R.string.ssid_label), new String()));
		payloadConfig.add(new AbstractActionData(context.getResources().getString(R.string.passkey_label), new String()));
		category = ActionsCategories.CONNECTIVITY.getLabel(context);
		icon = "ic_network_wifi_add";
		modificationAuthorizations = new boolean[] { false, false };
		title = context.getResources().getString(R.string.wifiadd_action_title);
	}
	
	@Override
	public void execute(Context context, String param[]) {
		connectToAP(context, param[0].trim(), param[1].trim());
	}
	
	private void connectToAP(Context context, String ssid, String passkey) {
		boolean found = false;
		
	    WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
	    WifiConfiguration wifiConfiguration = new WifiConfiguration();

	    boolean setOn = !wifiManager.isWifiEnabled();
		
	    wifiManager.setWifiEnabled(setOn);
	    
	    String networkSSID = ssid;
	    String networkPass = passkey;

	    for (ScanResult result : wifiManager.getScanResults()) {
	        if (result.SSID.equals(networkSSID)) {
	        	found = true;
	        	
	            String securityMode = getScanResultSecurity(result);

	            if (securityMode.equalsIgnoreCase("OPEN")) {

	                wifiConfiguration.SSID = "\"" + networkSSID + "\"";
	                wifiConfiguration.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
	                int res = wifiManager.addNetwork(wifiConfiguration);

	                wifiManager.enableNetwork(res, true);
	                wifiManager.setWifiEnabled(true);

	            } else if (securityMode.equalsIgnoreCase("WEP")) {

	                wifiConfiguration.SSID = "\"" + networkSSID + "\"";
	                wifiConfiguration.wepKeys[0] = "\"" + networkPass + "\"";
	                wifiConfiguration.wepTxKeyIndex = 0;
	                wifiConfiguration.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
	                wifiConfiguration.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP40);
	                int res = wifiManager.addNetwork(wifiConfiguration);
	                
	                wifiManager.enableNetwork(res, true);
	                wifiManager.setWifiEnabled(true);
	            }

	            wifiConfiguration.SSID = "\"" + networkSSID + "\"";
	            wifiConfiguration.preSharedKey = "\"" + networkPass + "\"";
	            wifiConfiguration.hiddenSSID = true;
	            wifiConfiguration.status = WifiConfiguration.Status.ENABLED;
	            wifiConfiguration.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
	            wifiConfiguration.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
	            wifiConfiguration.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
	            wifiConfiguration.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.TKIP);
	            wifiConfiguration.allowedPairwiseCiphers.set(WifiConfiguration.PairwiseCipher.CCMP);
	            wifiConfiguration.allowedProtocols.set(WifiConfiguration.Protocol.RSN);
	            wifiConfiguration.allowedProtocols.set(WifiConfiguration.Protocol.WPA);

	            int res = wifiManager.addNetwork(wifiConfiguration);

	            wifiManager.enableNetwork(res, true);

	            boolean changeHappen = wifiManager.saveConfiguration();

	            if(res != -1 && changeHappen){
//	                AppStaticVar.connectedSsidName = networkSSID;
	            }

	            wifiManager.setWifiEnabled(true);
	        }
	    }
	    
	    if (!found) {
	    	Toast.makeText(context, "Network not found", Toast.LENGTH_SHORT).show();
	    }
	}

	private String getScanResultSecurity(ScanResult scanResult) {
	    final String cap = scanResult.capabilities;
	    final String[] securityModes = { "WEP", "PSK", "EAP" };

	    for (int i = securityModes.length - 1; i >= 0; i--) {
	        if (cap.contains(securityModes[i])) {
	            return securityModes[i];
	        }
	    }

	    return "OPEN";
	}
}
