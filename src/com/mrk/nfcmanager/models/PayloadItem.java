package com.mrk.nfcmanager.models;

import com.mrk.mrkframework.model.ModelInterface;

public class PayloadItem implements ModelInterface {

	private int size;
	private byte actionType;
	private byte[] payload;
	
	public PayloadItem() {
		// default constructor
	}
	
	public PayloadItem(byte size, byte actionType, byte[] payload) {
		this.size = size;
		this.actionType = actionType;
		this.payload = payload;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public byte getActionType() {
		return actionType;
	}

	public void setActionType(byte actionType) {
		this.actionType = actionType;
	}

	public byte[] getPayload() {
		return payload;
	}

	public void setPayload(byte[] payload) {
		this.payload = payload;
	}
	
	@Override
	public PayloadItem clone() {
		return null;
	}
}
