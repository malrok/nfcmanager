package com.mrk.nfcmanager.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.nfc.NdefMessage;

import com.mrk.mrkframework.model.ModelInterface;

public class NFCTag implements Serializable, ModelInterface {

	private static final long serialVersionUID = 3411037255054508781L;

	private String name = "";
	
	private String icon = "";
	
	private Drawable iconDrawable = null;
	
	/** fields from android.nfc.Tag */
	private byte[] id = null;
	
	private String[] techList = null;
	
	/** fields from android.nfc.tech.Ndef */
	private boolean canMakeReadOnly = false;
	
	private int maxSize = -1;
	
	private String type = null;
	
	private boolean isWritable = false;
	
	private List<NFCRecord> records = null;
	
	private boolean AARTag = false;

	private NdefMessage[] rawTagMessages;
	
	public NFCTag() {
		records = new ArrayList<NFCRecord>();
	}
	
	public NFCTag(NFCTag tag) {
		records = new ArrayList<NFCRecord>();
		
		this.name = tag.name;
		this.icon = tag.icon;
		this.id = tag.id;
		this.techList = tag.techList;
		this.canMakeReadOnly = tag.canMakeReadOnly;
		this.maxSize = tag.maxSize;
		this.type = tag.type;
		this.isWritable = tag.isWritable;
		this.AARTag = tag.AARTag;
		
		for (NFCRecord record : tag.records) {
			this.addRecord(new NFCRecord(record));
		}
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Drawable getIconDrawable(Context context) {
		if (iconDrawable == null && icon != null) {
			iconDrawable = context.getResources().getDrawable(context.getResources().getIdentifier(icon, "drawable", "com.mrk.nfcmanager"));
		}
		return iconDrawable;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public byte[] getId() {
		return id;
	}

	public void setId(byte[] id) {
		this.id = id;
	}

	public String[] getTechList() {
		return techList;
	}

	public void setTechList(String[] techList) {
		this.techList = techList;
	}

	public boolean isCanMakeReadOnly() {
		return canMakeReadOnly;
	}

	public void setCanMakeReadOnly(boolean canMakeReadOnly) {
		this.canMakeReadOnly = canMakeReadOnly;
	}

	public int getMaxSize() {
		return maxSize;
	}

	public void setMaxSize(int maxSize) {
		this.maxSize = maxSize;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public boolean isWritable() {
		return isWritable;
	}

	public void setWritable(boolean isWritable) {
		this.isWritable = isWritable;
	}
	
	public List<NFCRecord> getRecords() {
		return records;
	}

	public void setRecords(List<NFCRecord> actions) {
		this.records = actions;
	}
	
	public void addRecord(NFCRecord action) {
		this.records.add(action);
	}

	public boolean isAARTag() {
		return AARTag;
	}

	public void setAARTag(boolean aARTag) {
		AARTag = aARTag;
	}

	public NdefMessage[] getRawtTagMessages() {
		return rawTagMessages;
	}

	public void setRawTagMessages(NdefMessage[] rawtTagMessages) {
		this.rawTagMessages = rawtTagMessages;
	}

	@Override
	public NFCTag clone() {
		return null;
	}
}
