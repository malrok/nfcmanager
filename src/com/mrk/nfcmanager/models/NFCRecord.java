package com.mrk.nfcmanager.models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.nfc.NdefRecord;

import com.mrk.mrkframework.model.ModelInterface;
import com.mrk.nfcmanager.models.nfcactions.WKTextActionClass;
import com.mrk.nfcmanager.models.nfcactions.WKUriActionClass;
import com.mrk.nfcmanager.models.nfcactions.abstracts.AbstractActionData;
import com.mrk.nfcmanager.models.nfcactions.abstracts.EnumWrapper;
import com.mrk.nfcmanager.models.nfcactions.abstracts.InstalledAppsWrapper;
import com.mrk.nfcmanager.models.nfcactions.interfaces.ActionClassInterface;

public class NFCRecord implements Serializable, ModelInterface {

	private static final long serialVersionUID = 3814913321927158959L;

	private String category;

	private List<AbstractActionData> params;

	private short tnf;

	private byte[] type;

	private byte[] payload;

	private String mimeType;

	private byte[] actionType;

	private String icon;

	private String actionTitle;

	private Drawable iconDrawable;

	private int rank;

	private boolean isInternal;

	public NFCRecord() {
		category = "";
		icon = null;
		params = null;
		isInternal = true;
	}

	public NFCRecord(int rank, ActionClassInterface actionClass) {
		this.category = actionClass.getCategory();
		this.tnf = (Short) actionClass.getDefaultTnf();
		this.actionType = (byte[]) actionClass.getDefaultType();
		this.icon = actionClass.getIcon();
		this.rank = rank;
		this.params = actionClass.getPayloadConfig();
		this.actionTitle = actionClass.getTitle();
		this.isInternal = actionClass.isInternal();

		if (actionClass.getDefaultTnf() == NdefRecord.TNF_WELL_KNOWN) {
			if (this.actionType == WKTextActionClass.WK_TEXT) {
				this.type = NdefRecord.RTD_TEXT;
			} else if (this.actionType == WKUriActionClass.WK_URI) {
				this.type = NdefRecord.RTD_URI;
			}
		}
	}

	public NFCRecord(NFCRecord record) {
		this.category = record.category;
		this.params = record.params;
		this.tnf = record.tnf;
		this.type = record.type;
		this.payload = record.payload;
		this.mimeType = record.mimeType;
		this.actionType = record.actionType;
		this.icon = record.icon;
		this.rank = record.rank;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public List<AbstractActionData> getParams() {
		if (params == null) {
			return new ArrayList<AbstractActionData>();
		} else {
			return params;
		}
	}

	public String[] getStringParams() {
		if (this.params != null) {
			String[] params = new String[this.params.size()];

			for (int rank = 0; rank < this.params.size(); rank++) {
				AbstractActionData actionData = this.params.get(rank);
				if (actionData.getData() instanceof EnumWrapper<?>) {
					EnumWrapper<?> enumData = (EnumWrapper<?>)actionData.getData();
					params[rank] = enumData.getItems()[enumData.getSelectedItem()].toString();
				} else if (actionData.getData() instanceof InstalledAppsWrapper) {
					InstalledAppsWrapper appsData = (InstalledAppsWrapper)actionData.getData();
					params[rank] = appsData.getSelectedApp();
				} else if (actionData.getData() instanceof String) {
					params[rank] = actionData.getData().toString();
				}
			}

			return params;
		}

		return new String[0];
	}

	public void setParams(List<AbstractActionData> params) {
		if (params != null) {
			this.params = params;
		} else {
			this.params = null;
		}
	}

	public void addParam(AbstractActionData param) {
		this.params.add(param);
	}

	public short getTnf() {
		return tnf;
	}

	public void setTnf(short tnf) {
		this.tnf = tnf;
	}

	public byte[] getType() {
		return type;
	}

	public void setType(byte[] type) {
		this.type = type;
	}

	public byte[] getPayload() {
		return payload;
	}

	public void setPayload(byte[] payload) {
		this.payload = payload;
	}

	public String getMimeType() {
		return mimeType;
	}

	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}

	public byte[] getActionType() {
		return actionType;
	}

	public void setActionType(byte[] actionType) {
		this.actionType = actionType;
	}

	public Drawable getIconDrawable(Context context) {
		if (iconDrawable == null && icon != null) {
			iconDrawable = context.getResources().getDrawable(
					context.getResources().getIdentifier(icon, "drawable", "com.mrk.nfcmanager"));
		}
		return iconDrawable;
	}

	public String getIcon() {
		return this.icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}

	public String getActionTitle() {
		return actionTitle;
	}

	public void setActionTitle(String actionTitle) {
		this.actionTitle = actionTitle;
	}

	public int getRank() {
		return rank;
	}

	public void setRank(int rank) {
		this.rank = rank;
	}

	public boolean isInternal() {
		return isInternal;
	}

	public void setInternal(boolean isInternal) {
		this.isInternal = isInternal;
	}

	@Override
	public NFCRecord clone() {
		return null;
	}
}
