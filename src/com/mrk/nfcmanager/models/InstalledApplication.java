package com.mrk.nfcmanager.models;

import android.graphics.drawable.Drawable;

import com.mrk.mrkframework.model.ModelInterface;

public class InstalledApplication implements ModelInterface {

	private Drawable icon;
	private String title;
	private String pack;
	private String process;

	public Drawable getIcon() {
		return icon;
	}

	public void setIcon(Drawable icon) {
		this.icon = icon;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getPack() {
		return pack;
	}

	public void setPack(String pack) {
		this.pack = pack;
	}

	public String getProcess() {
		return process;
	}

	public void setProcess(String process) {
		this.process = process;
	}
	
	@Override
	public InstalledApplication clone() {
		return null;
	}
}
