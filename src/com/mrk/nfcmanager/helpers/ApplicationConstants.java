package com.mrk.nfcmanager.helpers;


public class ApplicationConstants {

	public final static String PACKAGE = "com.mrk.nfcmanager";
	public final static String AAR_TYPE = "android.com:pkg";
	public final static String APP_NDEF_TYPE = "malrok.com:nfcmanager";
	public final static String NDEF_MIME_TYPE = "application/" + PACKAGE;
	
}
