package com.mrk.nfcmanager.helpers;

public class BytesArrayHelper {

	byte[] bytesArray = new byte[0];
	
	/** 
	 * For numbers inferior to 255
	 * @param addLength
	 * @param data
	 */
	public void addIntegerToArray(boolean addLength, int data) {
		byte[] dataBytes = new byte[4];
		
	    for (int i = 0; i < 4; i++) {
	        int offset = (dataBytes.length - 1 - i) * 8;
	        dataBytes[i] = (byte) ((data >>> offset) & 0xFF);
	    }

	    addBytesToArray(addLength, dataBytes);
	}
	
	/**
	 * Adds int in 4 positions
	 * @param addLength
	 * @param data
	 */
	public void addSmallintToArray(boolean addLength, int data) {
		byte[] dataBytes = new byte[1];
		
        int offset = (dataBytes.length - 1) * 8;
        dataBytes[0] = (byte) ((data >>> offset) & 0xFF);

	    addBytesToArray(addLength, dataBytes);
	}
	
	/**
	 * adds a string
	 * @param addLength
	 * @param data
	 */
	public void addStringToArray(boolean addLength, String data) {
		byte[] dataBytes = data.getBytes();
		
		addBytesToArray(addLength, dataBytes);
	}
	
	/** 
	 * adds a byte array
	 * @param addLength
	 * @param data
	 */
	public void addBytesToArray(boolean addLength, byte[] data) {
		increaseArrayLength(data.length);
		
		if (addLength) {
			increaseArrayLength(1);
			System.arraycopy(new byte[]{ (byte)(data.length & 0xff) }, 0, bytesArray, bytesArray.length - data.length - 1, 1);
		}
		
		System.arraycopy(data, 0, bytesArray, bytesArray.length - data.length, data.length);
	}
	
	private void increaseArrayLength(int size) {
		byte[] oldOne = bytesArray.clone();
		bytesArray = new byte[oldOne.length + size];
		System.arraycopy(oldOne, 0, bytesArray, 0, oldOne.length);
	}
	
	public byte[] getBytesArray() {
		return bytesArray;
	}
	
	public static byte[] subArray(byte[] array, int start, int end) {
		byte[] result = new byte[end - start];
		
		System.arraycopy(array, start, result, 0, end - start);
		
		return result;
	}
	
	public static int byteArrayToInt(byte[] array) {
		int res = 0, shift = 0;
		
		for (int rank=array.length - 1; rank >= 0 ; rank--) {
			res+= (array[rank] & 0xff) << (shift++ * rank); 
		}
		
		return res;
	}
}
