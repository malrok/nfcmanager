package com.mrk.nfcmanager.helpers;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;

import com.mrk.nfcmanager.R;

public enum ActionsCategories {
	
	CONNECTIVITY(0),
	MEDIA(1),
	WELLKNOWN(2),
	EXECUTIONS(3);
	
	private static List<String> categories;

	private int value;
	
	private ActionsCategories(int value) {
		this.value = value;
	}	

	public int getValue() {
		return this.value;
	}
	
	public String getLabel(Context context) {
		fillCategories(context);
		
		return categories.get(this.value);
	}
	
	public static List<String> getCategories(Context context) {
		fillCategories(context);
		
		return categories;
	}
	
	public static int getidForCategory(Context context, String category) {
		fillCategories(context);
		
		return categories.indexOf(category);
	}
	
	public static String getLabelForCategory(Context context, int category) {
		fillCategories(context);
		
		return categories.get(category);
	}
	
	private static void fillCategories(Context context) {
		if (categories == null) {
			categories = new ArrayList<String>();
			categories.add(context.getResources().getString(R.string.connectivity_category));
			categories.add(context.getResources().getString(R.string.media_category));
			categories.add(context.getResources().getString(R.string.wellknown_category));
			categories.add(context.getResources().getString(R.string.execution_category));
		}
	}
}