package com.mrk.nfcmanager.helpers;

import java.util.Random;

import android.content.Context;
import android.view.Gravity;

import com.mrk.nfcmanager.R;

public class ActionViewRankHelper {

	private static ActionViewRankHelper instance;
	private static int[] colors;
	private static int lastColor = 0;
	private static int lastGravity = 0;
	
	public static ActionViewRankHelper getInstance(Context context) {
	    if (null == instance) {
	    	int rank = 0;
	    	
	    	String[] colorsArray = context.getResources().getStringArray(R.array.colors);
	    	
	    	colors = new int[colorsArray.length];
	    	
	    	for (String color : colorsArray) {
	    		colors[rank] = context.getResources().getColor(context.getResources().getIdentifier(color, "color", "com.mrk.nfcmanager"));
	    		rank++;
	    	}
	        
	    	instance = new ActionViewRankHelper();
	    }
	    return instance;
	}
	
	private ActionViewRankHelper() {
	}
	
	public int getNextColor() {	
		int nextColor = 0;
		
		do {
			nextColor = colors[new Random().nextInt(colors.length - 1)];
		} while (nextColor == lastColor);
		
		lastColor = nextColor;
		
		return nextColor;
	}
	
	public int getNextGravity() {
		int nextGravity = 0, newGrav;
		
		do {
			newGrav = new Random().nextInt(3);
			switch(newGrav) {
			case 0:
				nextGravity = Gravity.LEFT | Gravity.TOP;
				break;
			case 1:
				nextGravity = Gravity.RIGHT | Gravity.TOP;
				break;
			case 2:
				nextGravity = Gravity.LEFT | Gravity.BOTTOM;
				break;
			case 3:
				nextGravity = Gravity.RIGHT | Gravity.BOTTOM;
				break;
			}
		} while (nextGravity == lastGravity);
		
		lastGravity = nextGravity;
		
		return nextGravity;
	}
	
}
