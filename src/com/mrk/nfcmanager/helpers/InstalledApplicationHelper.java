package com.mrk.nfcmanager.helpers;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.AsyncTask;

import com.mrk.nfcmanager.models.InstalledApplication;
import com.mrk.nfcmanager.ui.views.ActionViewMenuItemHolder;

public class InstalledApplicationHelper {

	private Context context;
	private static InstalledApplicationHelper instance;
	private List<InstalledApplication> apps;
	private List<FilteredMenuItemHolder> demandQueue;
	private boolean doneLoading = false;
	
	private InstalledApplicationHelper(Context context) {
		this.context = context;
		
		ReadAppsList readList = new ReadAppsList();
		readList.execute();
		
		demandQueue = new ArrayList<FilteredMenuItemHolder>(); 
	}
	
	public static InstalledApplicationHelper getInstance(Context context) {
		if (instance == null) {
			instance = new InstalledApplicationHelper(context);
		}
		return instance;
	}
	
	public static void launchAppFromPackage(Context context, String pack) {
		PackageManager packageManager = context.getApplicationContext().getPackageManager();

		Intent startupIntent = packageManager.getLaunchIntentForPackage(pack);
		startupIntent.setAction(Intent.ACTION_MAIN);
		startupIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		
		context.startActivity(startupIntent);
	}
	
	public List<InstalledApplication> getApplicationsList(ActionViewMenuItemHolder actionHolder, List<String> filter) {
		if (!doneLoading) {
			demandQueue.add(new FilteredMenuItemHolder(actionHolder, filter));
		}
		return getFilteredApps(filter);
	}

	public int getPackageIndex(String selectedApp) {
		if (doneLoading) {
			for (int rank = 0 ; rank < apps.size() ; rank++) {
				if (apps.get(rank).getPack().equals(selectedApp)) {
					return rank;
				}
			}
		}
		return 0;
	}
	
	private class ReadAppsList extends AsyncTask<Void, Void , Void> {

		@Override
		protected void onPostExecute(Void result) {
			for (FilteredMenuItemHolder filteredHolder : demandQueue) {
				filteredHolder.getHolder().updateSpinnerAdapter(getFilteredApps(filteredHolder.getFilter()));
			}
			demandQueue.clear();
			doneLoading = true;
		}
		
		@Override
		protected Void doInBackground(Void... params) {
			apps = new ArrayList<InstalledApplication>();

			Intent mainIntent = new Intent(Intent.ACTION_MAIN, null);
			mainIntent.addCategory(Intent.CATEGORY_LAUNCHER);

			PackageManager packageManager = context.getPackageManager();
			List<ResolveInfo> list = packageManager.queryIntentActivities(mainIntent, 0);

			for (ResolveInfo rInfo : list) {
				InstalledApplication application = new InstalledApplication();

				application.setIcon(rInfo.activityInfo.loadIcon(packageManager));
				application.setTitle(rInfo.activityInfo.loadLabel(packageManager).toString());
				application.setPack(rInfo.activityInfo.packageName);
				application.setProcess(rInfo.activityInfo.name);

				apps.add(application);
			}
			
			sortList();
			
			return null;
		}
	}

	private void sortList() {
		Comparator<InstalledApplication> appsComparator = new Comparator<InstalledApplication>() {
			public int compare(InstalledApplication ap1, InstalledApplication ap2){
				return ap1.getTitle().compareToIgnoreCase(ap2.getTitle());
			}      
		};
		
		Collections.sort(apps, appsComparator);
	}

	private List<InstalledApplication> getFilteredApps(List<String> filter) {
		List<InstalledApplication> filteredApps = this.apps;

		if (filter != null) {
			filteredApps = new ArrayList<InstalledApplication>();
			
			for (InstalledApplication app : this.apps) {
				if (filter.contains(app.getPack())) {
					filteredApps.add(app);
				}
			}
		}
		
		return filteredApps;
	}

	private static class FilteredMenuItemHolder {
		private ActionViewMenuItemHolder holder;

		private List<String> filter;
		
		public FilteredMenuItemHolder(ActionViewMenuItemHolder holder, List<String> filter) {
			this.holder = holder;
			this.filter = filter;
		}
		
		public ActionViewMenuItemHolder getHolder() {
			return holder;
		}

		public List<String> getFilter() {
			return filter;
		}
	}
}
