package com.mrk.nfcmanager.helpers;

import java.util.ArrayList;
import java.util.List;

import com.mrk.nfcmanager.models.PayloadItem;

public class ActionsHelper {
	
	public static List<PayloadItem> getPayloadItemsFromPayload(byte[] payload) {
		List<PayloadItem> result = new ArrayList<PayloadItem>();
		
		int reachedPos = 0;
		
		while (reachedPos < payload.length) {
			PayloadItem item = new PayloadItem();
			
			int length = payload[reachedPos++];
			
			try {
				byte[] work;
				
				// type;
				work = new byte[1];
				System.arraycopy(payload, reachedPos++, work, 0, 1);
				item.setActionType(work[0]);
				
				// payload
				work = new byte[length - 1];
				System.arraycopy(payload, reachedPos, work, 0, length - 1);
				item.setPayload(work);
				
				reachedPos += length - 1;
				
			} catch (Exception e) {
				e.printStackTrace();
				continue;
			}
			
			result.add(item);
		}
		
		return result;
	}
}
