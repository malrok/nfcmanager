package com.mrk.nfcmanager.viewmodels;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.nfc.NdefRecord;

import com.mrk.mrkframework.dataloaders.interfaces.IDataLoader;
import com.mrk.mrkframework.viewmodel.abstracts.AbstractListViewModel;
import com.mrk.mrkframework.viewmodel.interfaces.IViewModel;
import com.mrk.nfcmanager.helpers.ApplicationConstants;
import com.mrk.nfcmanager.models.NFCRecord;

public class RecordsListViewModel extends AbstractListViewModel<NFCRecord> {

	@Override
	public void clear() {
		this.listData = new ArrayList<IViewModel<NFCRecord>>();
	}

	@Override
	public void loadFromDataLoader(Context context, IDataLoader<NFCRecord> dataLoader) {
		clear();
		
		List<NFCRecord> dataList = dataLoader.getData(context);
		
		for (NFCRecord data : dataList) {
			RecordViewModel vm = new RecordViewModel(context);
			vm.loadFromData(data);
			this.listData.add(vm);
		}
	}
	
	public void addActionViewModel(IViewModel<NFCRecord> vm) {
		this.listData.add(vm);
	}

	public List<IViewModel<NFCRecord>> getListData(boolean fullList) {
		if (fullList) {
			return listData;
		} else {
			List<IViewModel<NFCRecord>> list = new ArrayList<IViewModel<NFCRecord>>();
			
			for (IViewModel<NFCRecord> recordVM : listData) {
				boolean isAAR = (recordVM.getData().getType()!= null && new String(recordVM.getData().getType()).equals(ApplicationConstants.AAR_TYPE));
				
				if (!isAAR && recordVM.getData().getTnf() != NdefRecord.TNF_EMPTY) {
					list.add(recordVM);
				}
			}
			
			return list;
		}
	}
	
}
