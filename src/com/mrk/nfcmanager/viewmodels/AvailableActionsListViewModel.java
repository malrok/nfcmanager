package com.mrk.nfcmanager.viewmodels;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import android.content.Context;

import com.mrk.mrkframework.dataloaders.interfaces.IDataLoader;
import com.mrk.mrkframework.viewmodel.abstracts.AbstractListViewModel;
import com.mrk.mrkframework.viewmodel.interfaces.IViewModel;
import com.mrk.nfcmanager.models.NFCRecord;

public class AvailableActionsListViewModel extends AbstractListViewModel<NFCRecord> {

	private Map<String, Integer> categoriesMap;
	
	public AvailableActionsListViewModel() {
		// nothing to do
	}
	
	@Override
	public void clear() {
		// nothing to do
	}

	@Override
	public void loadFromDataLoader(Context context, IDataLoader<NFCRecord> dataLoader) {
		List<NFCRecord> data = dataLoader.getData(context);
		
		for (NFCRecord action : data) {
			RecordViewModel vm = new RecordViewModel(context);
			vm.loadFromData(action);
			this.listData.add(vm);
		}
		sortList();
	}
	
	private void sortList() {
		Comparator<IViewModel<NFCRecord>> appsComparator = new Comparator<IViewModel<NFCRecord>>() {
			public int compare(IViewModel<NFCRecord> vm1, IViewModel<NFCRecord> vm2){
				return vm1.getData().getCategory().compareToIgnoreCase(vm2.getData().getCategory());
			}      
		};
		
		Collections.sort(this.listData, appsComparator);
	}
	
	public Map<String, Integer> getCategoriesMap() {
		if (this.categoriesMap == null) {
			this.categoriesMap = new HashMap<String, Integer>();
			for (IViewModel<NFCRecord> vm : this.listData) {
				if (this.categoriesMap.containsKey(vm.getData().getCategory())) {
					int num = this.categoriesMap.get(vm.getData().getCategory()) + 1;
					this.categoriesMap.put(vm.getData().getCategory(), num);
				} else {
					this.categoriesMap.put(vm.getData().getCategory(), 1);
				}
			}
			
			this.categoriesMap = sortByKeys(this.categoriesMap);
		}
		return this.categoriesMap;
	}
	
	/*
     * Paramterized method to sort Map e.g. HashMap or Hashtable in Java
     * throw NullPointerException if Map contains null key
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
	private static <K extends Comparable,V extends Comparable> Map<K,V> sortByKeys(Map<K,V> map){
        List<K> keys = new LinkedList<K>(map.keySet());
        Collections.sort(keys);
     
        //LinkedHashMap will keep the keys in the order they are inserted
        //which is currently sorted on natural ordering
        Map<K,V> sortedMap = new LinkedHashMap<K,V>();
        for(K key: keys){
            sortedMap.put(key, map.get(key));
        }
     
        return sortedMap;
    }
}
