package com.mrk.nfcmanager.viewmodels;

import android.content.Context;
import android.nfc.NdefMessage;

import com.mrk.mrkframework.viewmodel.abstracts.AbstractViewModel;
import com.mrk.mrkframework.viewmodel.interfaces.IViewModel;
import com.mrk.nfcmanager.helpers.ApplicationConstants;
import com.mrk.nfcmanager.models.NFCRecord;
import com.mrk.nfcmanager.models.NFCTag;

public class TagViewModel extends AbstractViewModel<NFCTag> {
	
	private RecordsListViewModel recordsList;
	
	private NdefMessage[] rawTagMessages;
	
	private Context context;
	
	private boolean AARTag = false;
	
	public TagViewModel(Context context) {
		this.context = context;
		
		clear();
	}
	
	public TagViewModel clone(TagViewModel actionVM) {
		TagViewModel clonedVM = new TagViewModel(context);
		
		clonedVM.getData().setName(actionVM.getData().getName());
		clonedVM.getData().setIcon(actionVM.getData().getIcon());
		clonedVM.getData().setId(actionVM.getData().getId());
		clonedVM.getData().setTechList(actionVM.getData().getTechList());
		clonedVM.getData().setCanMakeReadOnly(actionVM.getData().isCanMakeReadOnly());
		clonedVM.getData().setMaxSize(actionVM.getData().getMaxSize());
		clonedVM.getData().setType(actionVM.getData().getType());
		clonedVM.getData().setWritable(actionVM.getData().isWritable());
		
		for (NFCRecord action : actionVM.getData().getRecords()) {
			RecordViewModel vm = new RecordViewModel(context);
			vm.loadFromData(action);
			clonedVM.recordsList.addActionViewModel(vm);
		}
		
		return clonedVM;
	}
	
	@Override
	public NFCTag getData() {
		this.data.setRecords(recordsList.getData());
		return this.data;
	}
	
	@Override
	public void loadFromData(NFCTag tag) {
		this.data.setName(tag.getName());
		this.data.setIcon(tag.getIcon());
		this.data.setId(tag.getId());
		this.data.setTechList(tag.getTechList());
		this.data.setCanMakeReadOnly(tag.isCanMakeReadOnly());
		this.data.setMaxSize(tag.getMaxSize());
		this.data.setType(tag.getType());
		this.data.setWritable(tag.isWritable());
		this.rawTagMessages = tag.getRawtTagMessages();
		this.AARTag = tag.isAARTag();
		
		this.recordsList  = new RecordsListViewModel();
		
		for (NFCRecord record : tag.getRecords()) {
			RecordViewModel vm = new RecordViewModel(context);
			vm.loadFromData(record);
			this.recordsList.addActionViewModel(vm);
		}
		
		this.data.setRecords(this.recordsList.getData());
	}
	
	public RecordsListViewModel getRecordsList() {
		return recordsList;
	}

	public void setRecordsList(RecordsListViewModel actionsList) {
		this.recordsList = actionsList;
	}
	
	public void addRecordToList(RecordViewModel vm) {
		vm.getData().setRank(this.recordsList.getData().size() + 1);
		this.recordsList.addActionViewModel(vm);
	}
	
	public void updateRecordsRank() {
		int rank = 0;
		for (IViewModel<NFCRecord> item : this.getRecordsList().getListData()) {
			item.getData().setRank(rank + 1);
			rank++;
		}
	}
	
	public String getTagId() {
		String tagId = "";
		
		if (data.getId() != null) {
			tagId = byteArrayToHexString(data.getId());
		}
		
		return tagId;
	}
	
	public String getTagTechs() {
		String techs = "";
		
		if (data.getTechList() != null) {
			for (String tech : data.getTechList()) {
				if (techs.length() > 0) {
					techs = techs + " / ";
				}
				techs += tech.substring(tech.lastIndexOf(".") + 1);
			}
		}
		
		return techs;
	}
	
	public String getTagSize() {
		return Integer.toString(data.getMaxSize());
	}
	
	public String getTagType() {
		return data.getType();
	}
	
	public String getTagReadonly() {
		return Boolean.toString(data.isCanMakeReadOnly());
	}
	
	private String byteArrayToHexString(byte[] array) {
		StringBuffer hexString = new StringBuffer();
		
		for (byte b : array) {
			if (hexString.length() > 0) {
				hexString.append(":");
			}
			int intVal = b & 0xff;
			if (intVal < 0x10) {
				hexString.append("0");
			}
			hexString.append(Integer.toHexString(intVal));
		}
		
		return hexString.toString();    
	}

	public void clear() {
		this.data = new NFCTag();
		
		this.data.setName("");
		this.data.setIcon("");
		
		recordsList  = new RecordsListViewModel();
	}

	public boolean isAARTag() {
		for (NFCRecord record : recordsList.getData()) {
			if (record.getType() != null && record.getPayload() != null) {
				String type = new String(record.getType());
				String payload = new String(record.getPayload());
				
				AARTag |= (type.equals(ApplicationConstants.AAR_TYPE) && payload.equals(ApplicationConstants.PACKAGE));
			}
		}
		return AARTag;
	}

	public NdefMessage[] getRawTagMessages() {
		return rawTagMessages;
	}

	public void setRawTagMessages(NdefMessage[] rawTagMessages) {
		this.rawTagMessages = rawTagMessages;
	}
}
