package com.mrk.nfcmanager.viewmodels;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import android.content.Context;

import com.mrk.mrkframework.viewmodel.abstracts.AbstractViewModel;
import com.mrk.nfcmanager.helpers.BytesArrayHelper;
import com.mrk.nfcmanager.models.nfcactions.abstracts.AbstractActionClass;
import com.mrk.nfcmanager.models.nfcactions.abstracts.AbstractActionData;
import com.mrk.nfcmanager.models.nfcactions.abstracts.EnumWrapper;
import com.mrk.nfcmanager.models.nfcactions.abstracts.InstalledAppsWrapper;

public class ActionClassViewModel extends AbstractViewModel<AbstractActionClass> {

	private boolean canModifyTnf;
	private boolean canModifyType;
	
	private short tnf;
	private byte[] type;
	private byte[] payload;

	public ActionClassViewModel(Context context, Class<?> actionClass) {
		try {
			Constructor<?> c = actionClass.getDeclaredConstructor(Context.class);
			c.setAccessible(true);
			this.data = (AbstractActionClass) c.newInstance(new Object[] {context});
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException e) {
			e.printStackTrace();
		}
	}
	
	public boolean canModifyTnf() {
		return canModifyTnf;
	}

	public void setCanModifyTnf(boolean canModifyNtf) {
		this.canModifyTnf = canModifyNtf;
	}

	public boolean canModifyType() {
		return canModifyType;
	}

	public void setCanModifyType(boolean canModifyType) {
		this.canModifyType = canModifyType;
	}

	public short getTnf() {
		return tnf;
	}

	public void setTnf(short tnf) {
		this.tnf = tnf;
	}

	public byte[] getType() {
		return type;
	}

	public void setType(byte[] type) {
		this.type = type;
	}

	public byte[] getPayload() {
		return payload;
	}
	
	public void computePayload() {
		BytesArrayHelper byteHelper = new BytesArrayHelper();
		
		for (int rank = 0 ; rank < this.data.getPayloadConfig().size() ; rank++) {
			AbstractActionData payloadPart = this.data.getPayloadConfig().get(rank);
			if (payloadPart.getData() instanceof EnumWrapper) {
				byteHelper.addIntegerToArray(true, ((EnumWrapper<?>)payloadPart.getData()).getSelectedItem());
			} else if (payloadPart.getData() instanceof InstalledAppsWrapper) {
				byteHelper.addStringToArray(true, ((InstalledAppsWrapper)payloadPart.getData()).getSelectedApp());
			} else {
				byteHelper.addStringToArray(true, payloadPart.getData().toString());
			}
		}
		
		this.payload = byteHelper.getBytesArray();
	}

	public void setPayload(byte[] payload) {
		this.payload = payload;
	}
}
