package com.mrk.nfcmanager.viewmodels;

import android.content.Context;

import com.mrk.mrkframework.viewmodel.abstracts.AbstractViewModel;
import com.mrk.mrkframework.viewmodel.interfaces.IViewModel;
import com.mrk.nfcmanager.models.NFCRecord;

public class RecordViewModel extends AbstractViewModel<NFCRecord> {

	private Context context;
	
	public RecordViewModel(Context context) {
		this.context = context;
		this.data = new NFCRecord();
	}
	
	public RecordViewModel clone(IViewModel<NFCRecord> actionVM) {
		RecordViewModel clonedVM = new RecordViewModel(context);
		
		clonedVM.getData().setCategory(actionVM.getData().getCategory());
		clonedVM.getData().setIcon(actionVM.getData().getIcon());
		clonedVM.getData().setParams(actionVM.getData().getParams());
		clonedVM.getData().setTnf(actionVM.getData().getTnf());
		clonedVM.getData().setType(actionVM.getData().getType());
		clonedVM.getData().setPayload(actionVM.getData().getPayload());
		clonedVM.getData().setMimeType(actionVM.getData().getMimeType());
		clonedVM.getData().setActionType(actionVM.getData().getActionType());
		clonedVM.getData().setInternal(actionVM.getData().isInternal());
		
		return clonedVM;
	}
	
	public void updateFromActionClassViewModel(ActionClassViewModel actionClassVM) {
		this.data.setTnf(actionClassVM.getTnf());
		this.data.setActionType(actionClassVM.getType());
		this.data.setPayload(actionClassVM.getPayload());
		this.data.setParams(actionClassVM.getData().getPayloadConfig());
	}
}
