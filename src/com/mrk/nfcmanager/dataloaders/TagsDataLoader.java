package com.mrk.nfcmanager.dataloaders;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.os.Parcelable;

import com.fasterxml.jackson.jr.ob.JSON;
import com.fasterxml.jackson.jr.ob.JSONObjectException;
import com.mrk.mrkframework.dal.FileManager;
import com.mrk.mrkframework.dataloaders.abstracts.AbstractDataLoader;
import com.mrk.nfcmanager.helpers.ActionsHelper;
import com.mrk.nfcmanager.helpers.ApplicationConstants;
import com.mrk.nfcmanager.helpers.BytesArrayHelper;
import com.mrk.nfcmanager.models.NFCRecord;
import com.mrk.nfcmanager.models.NFCTag;
import com.mrk.nfcmanager.models.PayloadItem;
import com.mrk.nfcmanager.models.nfcactions.WKTextActionClass;
import com.mrk.nfcmanager.models.nfcactions.WKUriActionClass;
import com.mrk.nfcmanager.models.nfcactions.abstracts.AbstractActionData;
import com.mrk.nfcmanager.models.nfcactions.abstracts.EnumWrapper;
import com.mrk.nfcmanager.models.nfcactions.abstracts.InstalledAppsWrapper;

public class TagsDataLoader extends AbstractDataLoader<NFCTag> {

	private static final String FILE = "settings.json";
	
	@Override
	public List<NFCTag> getData(Context context) {
		FileManager fm = new FileManager(context, FILE, "r");
		
		List<NFCTag> tags = new ArrayList<NFCTag>();
		
		try {
			tags = JSON.std.listOfFrom(NFCTag.class, fm.read());
		} catch (JSONObjectException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		fm.close();
		
		return tags;
	}

	public NFCTag getNFCTagFromIntent(Context context, Intent intent) {
		NFCTag tag = null;
		
		// get ndef from intent
		Ndef ndefTag = Ndef.get((Tag) intent.getParcelableExtra(NfcAdapter.EXTRA_TAG));
		
		// if ndef exists
		if (ndefTag != null) {
			tag = setNfcTagFromNdef(ndefTag);
			
			// get raw messages
	    	Parcelable[] rawMsgs = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
			
			if (rawMsgs != null) {
				// cast raw messages to NdefMessages
				NdefMessage[] messages = new NdefMessage[rawMsgs.length];
				for (int i = 0; i < rawMsgs.length; i++) {
					messages[i] = (NdefMessage) rawMsgs[i];
				}
				
				// we keep "raw" messages for reading fragment
				tag.setRawTagMessages(messages);
				
				// analyse messages and create NFCRecords
				getRecordsFromNdefMessages(context, tag, messages);
			}
			
			// sort records by rank
			Collections.sort(tag.getRecords(), recordComparator);
		}
		
		return tag;
	}
	
	@Override
	public void pushFrom(Context context, List<NFCTag> data) {
		FileManager fm = new FileManager(context, FILE, "w");
		
		try {
			String json = JSON.std.asString(data);
			fm.write(json);	
		} catch (JSONObjectException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		fm.close();
	}
	
	/**
	 * Sets NFCTag from Ndef
	 * @param ndefTag
	 * @return NFCTag
	 */
	private NFCTag setNfcTagFromNdef(Ndef ndefTag) {
		NFCTag tag;
		tag = new NFCTag();
		
		tag.setId(ndefTag.getTag().getId());
		tag.setTechList(ndefTag.getTag().getTechList());
		tag.setCanMakeReadOnly(ndefTag.canMakeReadOnly());
		tag.setMaxSize(ndefTag.getMaxSize());
		tag.setType(ndefTag.getType());
		tag.setWritable(ndefTag.isWritable());
		return tag;
	}
	
	private void getRecordsFromNdefMessages(Context context, NFCTag tag, NdefMessage[] messages) {
		int rank = 1;
		
		InternalActionsDataLoader actionDataloader = new InternalActionsDataLoader();
		
		// for each message
		for (NdefMessage message : messages) {
			// for each record in every message
			for (NdefRecord record : message.getRecords()) {
				// given record tnf
				switch (record.getTnf()) {
					case NdefRecord.TNF_ABSOLUTE_URI:
						break;
					case NdefRecord.TNF_EMPTY:
						break;
					case NdefRecord.TNF_EXTERNAL_TYPE:
						break;
					case NdefRecord.TNF_MIME_MEDIA:
						rank = processMimeType(context, tag, rank, record, actionDataloader);
						break;
					case NdefRecord.TNF_UNCHANGED:
						break;
					case NdefRecord.TNF_UNKNOWN:
						break;
					case NdefRecord.TNF_WELL_KNOWN:
						rank = processWellKnown(context, tag, rank, record, actionDataloader);
						break;
				}
			}
		}
	}

	// process TNF_MIME_MEDIA record
	private int processMimeType(Context context, NFCTag tag, int rank, NdefRecord record, InternalActionsDataLoader actionDataloader) {
		if (new String(record.getType()).equals(ApplicationConstants.NDEF_MIME_TYPE)) {
			// This is a record managed by NFCManager
			// The actions are stored in the payload
			// We analyse that and create an NFCRecord for each action found
			for (PayloadItem item : ActionsHelper.getPayloadItemsFromPayload(record.getPayload())) {
				NFCRecord nfcRecord = new NFCRecord();
				nfcRecord.setTnf(record.getTnf());
				nfcRecord.setType(record.getType());
				nfcRecord.setMimeType(record.toMimeType());
				nfcRecord.setRank(rank++);
				nfcRecord.setActionType(new byte[]{ item.getActionType() });
				nfcRecord.setPayload(item.getPayload());
				
				actionDataloader.updateRecordFromAction(context, nfcRecord);
				updateParamsFromPayload(item.getPayload(), nfcRecord, record);
				
				tag.setAARTag(true);
				tag.addRecord(nfcRecord);
			}
		}
		return rank;
	}

	private void updateParamsFromPayload(byte[] payload, NFCRecord nfcRecord, NdefRecord record) {
		if (nfcRecord.getParams() != null) {
			int reachedPosition = 0;
			for (AbstractActionData param : nfcRecord.getParams()) {
				if (nfcRecord.getTnf() == NdefRecord.TNF_WELL_KNOWN) {
					if (nfcRecord.getActionType() == WKTextActionClass.WK_TEXT) {
						try {
							String encoding = ((payload[0] & 0200) == 0) ? "UTF-8" : "UTF-16";
							int langageCodeLength = payload[0] & 0077;
							param.setData(new String(payload, langageCodeLength + 1, payload.length - langageCodeLength - 1, encoding));
						} catch (Exception e) {
							e.printStackTrace();
						}
					} else if (nfcRecord.getActionType() == WKUriActionClass.WK_URI) {
						param.setData(record.toUri().toString());
					}
				} else {
					byte[] content;
					if (param.getData() instanceof EnumWrapper) {
						content = BytesArrayHelper.subArray(payload, reachedPosition, reachedPosition + 1);
						reachedPosition++;
						((EnumWrapper<?>) param.getData()).setSelectedItem(BytesArrayHelper.byteArrayToInt(content));
					} else if (param.getData() instanceof InstalledAppsWrapper) {
						int size = Integer.valueOf((byte) payload[reachedPosition++]);
						content = BytesArrayHelper.subArray(payload, reachedPosition, reachedPosition + size);
						reachedPosition += size;
						((InstalledAppsWrapper) param.getData()).setSelectedApp(new String(content));
					} else {
						int size = Integer.valueOf((byte) payload[reachedPosition++]);
						content = BytesArrayHelper.subArray(payload, reachedPosition, reachedPosition + size);
						reachedPosition += size;
						param.setData(new String(content));
					}
				}
			}
		}
	}
	
	// process TNF_WELL_KNOWN record
	private int processWellKnown(Context context, NFCTag tag, int rank, NdefRecord record, InternalActionsDataLoader actionDataloader) {
		NFCRecord nfcRecord = new NFCRecord();
		
		nfcRecord.setTnf(record.getTnf());
		nfcRecord.setType(record.getType());
		nfcRecord.setMimeType(record.toMimeType());
		nfcRecord.setRank(rank++);
		
		if (Arrays.equals(record.getType(), NdefRecord.RTD_URI)) {
			nfcRecord.setActionType(WKUriActionClass.WK_URI);
		} else if (Arrays.equals(record.getType(), NdefRecord.RTD_TEXT)) {
			nfcRecord.setActionType(WKTextActionClass.WK_TEXT);
		} else if (Arrays.equals(record.getType(), NdefRecord.RTD_SMART_POSTER)) {
//			nfcRecord.setActionType(NFCConstants.WK_SMARTPOSTER);
		}
		
		nfcRecord.setPayload(record.getPayload());
		
		actionDataloader.updateRecordFromAction(context, nfcRecord);
		updateParamsFromPayload(record.getPayload(), nfcRecord, record);
		tag.addRecord(nfcRecord);
		
		return rank;
	}
	
	private Comparator<NFCRecord> recordComparator = new Comparator<NFCRecord>() {
		@Override
		public int compare(NFCRecord leftRecord, NFCRecord rightRecord) {
			if (leftRecord.getRank() > rightRecord.getRank())
				return 1;
			if (leftRecord.getRank() < rightRecord.getRank())
				return -1;
			return 0;
		}      
	};
}
