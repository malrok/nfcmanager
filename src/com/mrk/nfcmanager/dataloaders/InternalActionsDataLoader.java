package com.mrk.nfcmanager.dataloaders;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import android.content.Context;

import com.mrk.mrkframework.dataloaders.abstracts.AbstractDataLoader;
import com.mrk.nfcmanager.helpers.ActionsCategories;
import com.mrk.nfcmanager.models.NFCRecord;
import com.mrk.nfcmanager.models.nfcactions.AirplaneActionClass;
import com.mrk.nfcmanager.models.nfcactions.BluetoothActionClass;
import com.mrk.nfcmanager.models.nfcactions.DataActionClass;
import com.mrk.nfcmanager.models.nfcactions.LaunchAppActionClass;
import com.mrk.nfcmanager.models.nfcactions.PlayMusicActionClass;
import com.mrk.nfcmanager.models.nfcactions.VolumeActionClass;
import com.mrk.nfcmanager.models.nfcactions.WKTextActionClass;
import com.mrk.nfcmanager.models.nfcactions.WKUriActionClass;
import com.mrk.nfcmanager.models.nfcactions.WifiActionClass;
import com.mrk.nfcmanager.models.nfcactions.WifiAddActionClass;
import com.mrk.nfcmanager.models.nfcactions.WifiHotSpotActionClass;
import com.mrk.nfcmanager.models.nfcactions.abstracts.AbstractActionClass;
import com.mrk.nfcmanager.models.nfcactions.abstracts.AbstractActionData;
import com.mrk.nfcmanager.models.nfcactions.abstracts.EnumWrapper;
import com.mrk.nfcmanager.models.nfcactions.abstracts.InstalledAppsWrapper;
import com.mrk.nfcmanager.models.nfcactions.interfaces.ActionClassInterface;
import com.mrk.nfcmanager.viewmodels.ActionClassViewModel;
import com.mrk.nfcmanager.viewmodels.RecordViewModel;

public class InternalActionsDataLoader extends AbstractDataLoader<NFCRecord> {

	private Map<String, Class<?>> internalActionsMap;
	
	private Context context;
	
	public InternalActionsDataLoader() {
		internalActionsMap = new HashMap<String, Class<?>>();
		
		/* connectivity */
		internalActionsMap.put(new String(WifiActionClass.WIFI_TYPE), WifiActionClass.class);
		internalActionsMap.put(new String(BluetoothActionClass.BLUETOOTH_TYPE), BluetoothActionClass.class);
		internalActionsMap.put(new String(DataActionClass.DATA_TYPE), DataActionClass.class);
		internalActionsMap.put(new String(WifiAddActionClass.WIFI_ADD_TYPE), WifiAddActionClass.class);
		internalActionsMap.put(new String(WifiHotSpotActionClass.WIFI_HOTSPOT_TYPE), WifiHotSpotActionClass.class);
		
		if (android.os.Build.VERSION.SDK_INT <  android.os.Build.VERSION_CODES.JELLY_BEAN) {
			internalActionsMap.put(new String(AirplaneActionClass.AIRPLANE_TYPE), AirplaneActionClass.class);
		}
		
		/* volume */
		internalActionsMap.put(new String(VolumeActionClass.VOLUME_TYPE), VolumeActionClass.class);
		
		/* well known types */
		internalActionsMap.put(new String(WKTextActionClass.WK_TEXT), WKTextActionClass.class);
		internalActionsMap.put(new String(WKUriActionClass.WK_URI), WKUriActionClass.class);
		
		/* execution */
		internalActionsMap.put(new String(LaunchAppActionClass.LAUNCH_APP_TYPE), LaunchAppActionClass.class);
		internalActionsMap.put(new String(PlayMusicActionClass.MUSIC_TYPE), PlayMusicActionClass.class);
	}
	
	@Override
	public List<NFCRecord> getData(Context context) {
		List<NFCRecord> data = new ArrayList<NFCRecord>();
		
		this.context = context;
		
		for (Entry<String, Class<?>> internalAction : internalActionsMap.entrySet()) {
			data.add(new NFCRecord(-1, getActionFromType(context, internalAction.getKey().getBytes())));
		}
		
		// sort by category
		Collections.sort(data, recordComparator);
		
		return data;
	}

	@Override
	public void pushFrom(Context context, List<NFCRecord> data) {
		// Nothing to do		
	}
	
	public void updateRecordFromAction(Context context, NFCRecord record) {
		ActionClassInterface actionClass = this.getActionFromType(context, record.getActionType());
		
		if (actionClass != null) {
			record.setIcon(actionClass.getIcon());
			record.setParams(actionClass.getPayloadConfig());
		}
	}
	
	public ActionClassInterface getActionFromType(Context context, byte[] type) {
		String sType = new String(type);
		if (internalActionsMap.containsKey(sType)) {
			try {
				Constructor<?> c = internalActionsMap.get(sType).getDeclaredConstructor(Context.class);
				c.setAccessible(true);
				return (ActionClassInterface) c.newInstance(new Object[] {context});
			} catch (InstantiationException | IllegalAccessException | IllegalArgumentException| InvocationTargetException | NoSuchMethodException e) {
				e.printStackTrace();
			}
		}
		return null;
	}
	
	public ActionClassViewModel loadFromActionClass(Context context, ActionClassInterface actionClass, RecordViewModel recordVM) {
		ActionClassViewModel vm = loadFromActionClass(context, actionClass);
		
		for (int rank=0; rank < recordVM.getData().getParams().size(); rank++) {
			AbstractActionData recordData = recordVM.getData().getParams().get(rank);
			AbstractActionData vmData = vm.getData().getPayloadConfig().get(rank);
			
			if (recordData.getData().getClass().equals(vmData.getData().getClass())) {
				if (recordData.getData() instanceof EnumWrapper) {
					((EnumWrapper<?>)vmData.getData()).setSelectedItem(((EnumWrapper<?>)recordData.getData()).getSelectedItem());
				} else if (recordData.getData() instanceof InstalledAppsWrapper) {
					((InstalledAppsWrapper)vmData.getData()).setSelectedApp(((InstalledAppsWrapper)recordData.getData()).getSelectedApp());
				} else {
					vmData.setData(recordData.getData());
				}
			} else {
				// uh oh deglingo
			}
		}
		
		vm.setTnf(recordVM.getData().getTnf());
		vm.setType(recordVM.getData().getActionType());
		vm.setPayload(recordVM.getData().getPayload());
		
		return vm;
	}
	
	public ActionClassViewModel loadFromActionClass(Context context, ActionClassInterface actionClass) {
		ActionClassViewModel vm = new ActionClassViewModel(context, actionClass.getClass());
		
		vm.getData().setCategory(actionClass.getCategory());
		vm.getData().setPayloadConfig(actionClass.getPayloadConfig());
		vm.getData().setIcon(actionClass.getIcon());
		vm.getData().setTnfConfig(actionClass.getTnfConfig());
		vm.getData().setTypeConfig(actionClass.getTypeConfig());
		vm.getData().setDefaultTnf(actionClass.getDefaultTnf());
		vm.getData().setDefaultType(actionClass.getDefaultType());
		vm.getData().setIsInternal(actionClass.isInternal());
		vm.setTnf((Short) actionClass.getDefaultTnf());
		vm.setType((byte[]) actionClass.getDefaultType());
		vm.setCanModifyTnf(actionClass.getModificationAuthorizations()[AbstractActionClass.TNF_MODIFY]);
		vm.setCanModifyType(actionClass.getModificationAuthorizations()[AbstractActionClass.TYPE_MODIFY]);
		
		return vm;
	}
	
	Comparator<NFCRecord> recordComparator = new Comparator<NFCRecord>() {
		@Override
		public int compare(NFCRecord leftRecord, NFCRecord rightRecord) {
			if (ActionsCategories.getidForCategory(context, leftRecord.getCategory()) > ActionsCategories.getidForCategory(context, rightRecord.getCategory()))
				return 1;
			if (ActionsCategories.getidForCategory(context, leftRecord.getCategory()) < ActionsCategories.getidForCategory(context, rightRecord.getCategory()))
				return -1;
			return 0;
		}      
	};
}
