package com.mrk.nfcmanager.nfc;

import java.io.IOException;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.DialogInterface.OnDismissListener;
import android.content.Intent;
import android.nfc.FormatException;
import android.nfc.NdefMessage;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.widget.Toast;

import com.mrk.nfcmanager.R;
import com.mrk.nfcmanager.ui.HostScreen;
import com.mrk.nfcmanager.ui.dialogs.WaitDialog;

public class NFCHandler {

	private Activity activity;
	private NfcAdapter nfcAdapter;
	private NdefMessage message;
	private WaitDialog waitForNFCDialog;
	private HandlerStatus handlerStatus;
	
	public NFCHandler(final Activity activity) {
		this.activity = activity;
        nfcAdapter = NfcAdapter.getDefaultAdapter(activity);
        
		waitForNFCDialog = new WaitDialog(activity, activity.getResources().getString(R.string.approach_message));

		handlerStatus = HandlerStatus.PENDING;
	}

	public void setDialogDismissInterface(OnDismissListener listener) {
		waitForNFCDialog.setOnDismissListener(listener);
	}
	
	public void enableNdefExchangeMode(PendingIntent pendingIntent) {
		nfcAdapter.enableForegroundDispatch(activity, pendingIntent, null, null);
	}
	
	public void disableNdefExchangeMode() {
		nfcAdapter.disableForegroundDispatch(activity);
		
		if (handlerStatus == HandlerStatus.WAITING_TO_WRITE) {
			nfcAdapter.setNdefPushMessage(null, activity);
		}
	}
	
	public NFCStatus checkNfc() {
		NFCStatus nfcStatus = NFCStatus.OK;
		
		if (nfcAdapter == null) {
			nfcStatus = NFCStatus.NFCUnable;
        } else if(!nfcAdapter.isEnabled()) {
        	nfcStatus = NFCStatus.NFCDisabled;
		}
		
		return nfcStatus;
	}
	
	public HandlerStatus getHandlerStatus() {
        return handlerStatus;
    }
    
	public void setPending() {
		if (waitForNFCDialog.isShowing()) {
			waitForNFCDialog.dismiss();
		}
		this.handlerStatus = HandlerStatus.PENDING;
	}
	
	public void prepareDelete(String title, NdefMessage message, boolean verbose) {
		this.prepareWriting(title, message, verbose);
		this.handlerStatus = HandlerStatus.WAITING_TO_DELETE;
	}
	
	public void prepareWriting(String title, NdefMessage message, boolean verbose) {
		if (verbose) {
			showDialogWithTitle(title);
		}
		this.message = message;
		this.handlerStatus = HandlerStatus.WAITING_TO_WRITE;
		nfcAdapter.setNdefPushMessage(message, activity);
	}

	public void write(final HostScreen activity, Intent intent, final NfcWriteCallback callback) {
		if (handlerStatus == HandlerStatus.WAITING_TO_WRITE || handlerStatus == HandlerStatus.WAITING_TO_DELETE) {
			Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
			final Ndef ndef = Ndef.get(tag);

			new Thread(new Runnable() {

				@Override
				public void run() {
					boolean error = false;
					
					try {
						ndef.connect();
						ndef.writeNdefMessage(message);
					} catch (IOException | FormatException e) {
						e.printStackTrace();
						error= true;
					}

					if (error) {
						activity.runOnUiThread(new Runnable() {
							@Override
							public void run() {
								Toast.makeText(activity.getApplicationContext(), activity.getApplicationContext().getResources().getString(R.string.write_tag_exception), Toast.LENGTH_LONG).show();
							}
						});
					}
					
					if (waitForNFCDialog.isShowing()) {
						waitForNFCDialog.dismiss();
					}
					
					if (callback != null) {
						activity.runOnUiThread(new Runnable() {
							@Override
							public void run() {
								callback.doAfterWrite();
							}
						});
					}
				}
			}).start();
		}
	}
	
	public void prepareCopy(String title, boolean verbose) {
		this.prepareReading(title, verbose);
		this.handlerStatus = HandlerStatus.WAITING_FOR_COPY;
	}
	
	public void prepareReading(String title, boolean verbose) {
		if (verbose) {
			showDialogWithTitle(title);
		}
        this.handlerStatus = HandlerStatus.WAITING_TO_READ;
	}
	
	private void showDialogWithTitle(String title) {
		waitForNFCDialog.setTitle(title);
		if (!waitForNFCDialog.isShowing()) {
			waitForNFCDialog.show();
		}
	}
	
	public void doneReading() {
		if (waitForNFCDialog.isShowing()) {
			waitForNFCDialog.dismiss();
		}
        handlerStatus = HandlerStatus.PENDING;
	}
	
	public static enum HandlerStatus {
		PENDING,
		WAITING_TO_READ,
		WAITING_TO_WRITE, 
		WAITING_TO_DELETE,
		WAITING_FOR_COPY
	}
}
