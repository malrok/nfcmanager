package com.mrk.nfcmanager.nfc;

public enum NFCStatus {
	NFCUnable,
	NFCDisabled,
	OK
}
