package com.mrk.nfcmanager.nfc;

public interface NfcWriteCallback {

	public void doAfterWrite();
	
}
