package com.mrk.nfcmanager.nfc.factory;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import android.nfc.NdefMessage;
import android.nfc.NdefRecord;

import com.mrk.mrkframework.viewmodel.interfaces.IViewModel;
import com.mrk.nfcmanager.helpers.ApplicationConstants;
import com.mrk.nfcmanager.helpers.BytesArrayHelper;
import com.mrk.nfcmanager.models.NFCRecord;
import com.mrk.nfcmanager.models.nfcactions.abstracts.AbstractActionData;
import com.mrk.nfcmanager.models.nfcactions.abstracts.EnumWrapper;
import com.mrk.nfcmanager.models.nfcactions.abstracts.InstalledAppsWrapper;
import com.mrk.nfcmanager.viewmodels.TagViewModel;

public class NdefMessageFactory {
	
	private boolean hasAAR = false;
	
	public NdefMessage createFromViewModel(TagViewModel viewModel) {
		if (viewModel.getRecordsList().getListData().size() > 0) {
		
			List<NdefRecord> records = new ArrayList<NdefRecord>();
			
			records.addAll(createRecords(viewModel));
			
			if (hasAAR) {
				records.add(NdefRecord.createApplicationRecord(ApplicationConstants.PACKAGE));
			}
			
			return createMessage(records);
		} else {
			return null;
		}
	}
	
	/**
	 *  Encoding a text into a tag payload
	 * @param nfcRecord
	 * @return
	 */
	private List<NdefRecord> createRecords(TagViewModel viewModel) {
		
		List<NdefRecord> ndefRecords = new ArrayList<NdefRecord>();
		
		BytesArrayHelper innerRecordPayload = new BytesArrayHelper();
		
		for (IViewModel<NFCRecord> nfcRecordVM : viewModel.getRecordsList().getListData()) {
		
			NFCRecord nfcRecord = nfcRecordVM.getData();
			
			switch (nfcRecord.getTnf()) {
				case NdefRecord.TNF_ABSOLUTE_URI:
					break;
				case NdefRecord.TNF_EMPTY:
					ndefRecords.add(new NdefRecord(NdefRecord.TNF_EMPTY, null, null, null));
					break;
				case NdefRecord.TNF_EXTERNAL_TYPE:
					break;
				case NdefRecord.TNF_MIME_MEDIA:
					hasAAR = true;
					innerRecordPayload.addBytesToArray(true, mimeMediaPayload(nfcRecord));
					break;
				case NdefRecord.TNF_UNCHANGED:
					break;
				case NdefRecord.TNF_UNKNOWN:
					break;
				case NdefRecord.TNF_WELL_KNOWN:
					if (nfcRecord.getType().equals(NdefRecord.RTD_TEXT)) {
						ndefRecords.add(new NdefRecord(NdefRecord.TNF_WELL_KNOWN, NdefRecord.RTD_TEXT, new byte[0], textPayload(nfcRecord)));
					} else if (nfcRecord.getType().equals(NdefRecord.RTD_URI)) {
						String uri = nfcRecord.getParams().get(0).getData().toString();
						if (uri == null || uri.length() == 0) {
							uri = "http://";
						}
						ndefRecords.add(NdefRecord.createUri(uri));
					}
					break;
			}
		}
		
		if (innerRecordPayload.getBytesArray().length > 0) {
			ndefRecords.add(new NdefRecord(NdefRecord.TNF_MIME_MEDIA, ApplicationConstants.NDEF_MIME_TYPE.getBytes(), new byte[0], innerRecordPayload.getBytesArray()));
		}
		
		return ndefRecords;
	}
	
	/**
	 * get payload from TNF_MIME_MEDIA record
	 * @param record
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	private byte[] mimeMediaPayload(NFCRecord record) {
		BytesArrayHelper bytesHelper = new BytesArrayHelper();
		
		bytesHelper.addBytesToArray(false, record.getActionType());
		
		for (AbstractActionData data : record.getParams()) {
			if (data.getData() instanceof EnumWrapper) {
				bytesHelper.addSmallintToArray(false, ((EnumWrapper)data.getData()).getSelectedItem());
			} else if (data.getData() instanceof InstalledAppsWrapper) {
				bytesHelper.addStringToArray(true, ((InstalledAppsWrapper)data.getData()).getSelectedApp());
			} else {
				bytesHelper.addStringToArray(true, data.getData().toString());
			}
		}
		
		return bytesHelper.getBytesArray();
	}

	/**
	 * get payload from rtd_text record
	 * @param nfcRecord
	 * @return
	 */
	private byte[] textPayload(NFCRecord nfcRecord) {
		byte[] langBytes = Locale.ENGLISH.getLanguage().getBytes(Charset.forName("US-ASCII"));
		byte[] textBytes = nfcRecord.getParams().get(0).getData().toString().getBytes(Charset.forName("UTF-8"));
		char status = (char) (langBytes.length);
		byte[] data = new byte[1 + langBytes.length + textBytes.length];
		data[0] = (byte) status;
		System.arraycopy(langBytes, 0, data, 1, langBytes.length);
		System.arraycopy(textBytes, 0, data, 1 + langBytes.length, textBytes.length);
		return data;
	}
	
	/**
	 *  Create a message from a record
	 * @param records
	 * @return
	 */
	private NdefMessage createMessage(List<NdefRecord> recordsList) {
		if (recordsList.size() > 0) {
			NdefRecord[] records = new NdefRecord[recordsList.size()];
			
			for (int rank=0; rank < recordsList.size(); rank++) {
				records[rank] = recordsList.get(rank);
			}
			
			NdefMessage message = new NdefMessage(records);
			return message;
		} else {
			return null;
		}
	}
	
}
